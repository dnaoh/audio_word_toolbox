﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using Xceed.Wpf.Toolkit;

namespace audio_word_toolbox
{
    class ProcessIntegerUpDown : IntegerUpDown
    {
        public IntegerUpDownProperty OptionProperty { private set; get; }

        public ProcessIntegerUpDown(IntegerUpDownProperty integerUpDownProperty)
        {
            Height = 22;
            Width = 60;
            Margin = new Thickness(10, 0, 0, 0);
            ToolTip = integerUpDownProperty.Name;
            Watermark = integerUpDownProperty.Watermark;
            Maximum = integerUpDownProperty.Maximum;
            Minimum = integerUpDownProperty.Minimum;
            OptionProperty = integerUpDownProperty;
        }
    }
}
