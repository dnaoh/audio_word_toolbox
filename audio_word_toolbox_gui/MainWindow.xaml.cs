﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Xml.Linq;
using System.Runtime.InteropServices;
using System.ComponentModel;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using AudioWordEncode;
using DictionaryTrain;

namespace audio_word_toolbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // variables
        private string corpusExtension = "*.wav";
        private string targetExtension = "*.wav";
        private string consoleTextBoxTemp = "";
        private string dictionaryTextBoxTemp = "";
        private XDocument pipelineDoc;
        private LayerSetup pipelineLayerSetup;
        private ProcessInformation processInformation;
        private bool endWithError = false;
        private bool endWithWarrning = false;

        // properties
        private string corpusDir;
        private string CorpusDir
        {
            get
            {
                return corpusDir;
            }
            set
            {
                if (value == "")
                    corpusDir = null;
                else
                    corpusDir = value;
            }
        }
        private string dictionary;
        private string Dictionary
        {
            get
            {
                return dictionary;
            }
            set
            {
                if (value == "")
                    dictionary = null;
                else
                    dictionary = value;
            }
        }
        private string targetDir;
        private string TargetDir
        {
            get
            {
                return targetDir;
            }
            set
            {
                if (value == "")
                    targetDir = null;
                else
                    targetDir = value;
            }
        }
        private string outputDir;
        private string OutputDir
        {
            get
            {
                return outputDir;
            }
            set
            {
                if (value == "")
                    outputDir = null;
                else
                    outputDir = value;
            }
        }
        private string tempDir;
        private string TempDir
        {
            get
            {
                return tempDir;
            }
            set
            {
                tempDir = value;
                consoleTextBox.Text += String.Format("Directory for temporary files: {0}.\r\n", tempDir);
                consoleTextBox.ScrollToEnd();
            }
        }
        private string outputExtension;
        private string OutputExtension
        {
            get
            {
                return outputExtension;
            }
            set
            {
                outputExtension = value;
                if (outputExtension == "*.mat")
                {
                    outputExtensionMATManuItem.IsChecked = true;
                    outputExtensionCSVManuItem.IsChecked = false;
                }
                else if (outputExtension == "*.csv")
                {
                    outputExtensionMATManuItem.IsChecked = false;
                    outputExtensionCSVManuItem.IsChecked = true;
                }
                consoleTextBox.Text += String.Format("Output format: {0}.\r\n", outputExtension);
                consoleTextBox.ScrollToEnd();
            }
        }
        private bool fileExistOverwrite;
        private bool FileExistOverwrite
        {
            get
            {
                return fileExistOverwrite;
            }
            set
            {
                fileExistOverwrite = value;
                if (fileExistOverwrite)
                {
                    fileExistActionOverwriteManuItem.IsChecked = true;
                    fileExistActionSkipManuItem.IsChecked = false;
                    consoleTextBox.Text += String.Format("File exist action: overwrite files.\r\n");
                }
                else
                {
                    fileExistActionOverwriteManuItem.IsChecked = false;
                    fileExistActionSkipManuItem.IsChecked = true;
                    consoleTextBox.Text += String.Format("File exist action: skip files.\r\n");
                }
                consoleTextBox.ScrollToEnd();
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            // initialize settings
            TempDir = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "audio_word_toolbox_temp");
            OutputExtension = "*.mat";
            FileExistOverwrite = true;

            // read pipelineElementSetup
            pipelineLayerSetup = new LayerSetup();

            // add input process panel
            InputDockPanel inputProcessDockPanel = new InputDockPanel(pipelineLayerSetup);
            workAreaStackPanel.Children.Insert(0, inputProcessDockPanel);

            // add initial process dock panel
            ProcessDockPanel initialProcessDockPanel = new ProcessDockPanel(pipelineLayerSetup);
            initialProcessDockPanel.ProcessDockPanelMovedUp += processDockPanel_ProcessDockPanelMovedUp;
            initialProcessDockPanel.ProcessDockPanelMovedDown += processDockPanel_ProcessDockPanelMovedDown;
            initialProcessDockPanel.ProcessDockPanelDeleted += processDockPanel_ProcessDockPanelDeleted;
            workAreaStackPanel.Children.Insert(2, initialProcessDockPanel);
        }

        private void processDockPanel_ProcessDockPanelMovedUp(object sender, EventArgs e)
        {
            int oldIndex = workAreaStackPanel.Children.IndexOf((ProcessDockPanel)sender);
            int newIndex = oldIndex - 1;

            if (newIndex != 1)
            {
                ProcessDockPanel processDockPanel = (ProcessDockPanel)workAreaStackPanel.Children[oldIndex];
                workAreaStackPanel.Children.Remove((ProcessDockPanel)sender);
                workAreaStackPanel.Children.Insert(newIndex, processDockPanel);
            }
        }

        private void processDockPanel_ProcessDockPanelMovedDown(object sender, EventArgs e)
        {
            int oldIndex = workAreaStackPanel.Children.IndexOf((ProcessDockPanel)sender);
            int newIndex = oldIndex + 1;

            if (newIndex != (workAreaStackPanel.Children.Count - 1))
            {
                ProcessDockPanel processDockPanel = (ProcessDockPanel)workAreaStackPanel.Children[oldIndex];
                workAreaStackPanel.Children.Remove((ProcessDockPanel)sender);
                workAreaStackPanel.Children.Insert(newIndex, processDockPanel);
            }
        }

        private void processDockPanel_ProcessDockPanelDeleted(object sender, EventArgs e)
        {
            workAreaStackPanel.Children.Remove((ProcessDockPanel)sender);
        }

        private void corpusDirTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // upadate the corpusDir variable with the current text in the text box
            CorpusDir = corpusDirTextBox.Text;
        }

        private void dictionaryTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // upadate the dictionary variable with the current text in the text box
            Dictionary = dictionaryTextBox.Text;
        }

        private void targetDirTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // upadate the targetDir variable with the current text in the text box
            TargetDir = targetDirTextBox.Text;
        }

        private void outputDirTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            // upadate the outputDir variable with the current text in the text box
            OutputDir = outputDirTextBox.Text;
        }

        // add process
        private void addProcessBeforeButton_Click(object sender, RoutedEventArgs e)
        {
            ProcessDockPanel processDockPanel = new ProcessDockPanel(pipelineLayerSetup);
            processDockPanel.ProcessDockPanelMovedUp += processDockPanel_ProcessDockPanelMovedUp;
            processDockPanel.ProcessDockPanelMovedDown += processDockPanel_ProcessDockPanelMovedDown;
            processDockPanel.ProcessDockPanelDeleted += processDockPanel_ProcessDockPanelDeleted;
            workAreaStackPanel.Children.Insert(2, processDockPanel);
        }

        private void addProcessAfterButton_Click(object sender, RoutedEventArgs e)
        {
            ProcessDockPanel processDockPanel = new ProcessDockPanel(pipelineLayerSetup);
            processDockPanel.ProcessDockPanelMovedUp += processDockPanel_ProcessDockPanelMovedUp;
            processDockPanel.ProcessDockPanelMovedDown += processDockPanel_ProcessDockPanelMovedDown;
            processDockPanel.ProcessDockPanelDeleted += processDockPanel_ProcessDockPanelDeleted;
            int workAreaStackPanelCount = workAreaStackPanel.Children.Count;
            workAreaStackPanel.Children.Insert(workAreaStackPanelCount - 1, processDockPanel);
        }

        // get dir with buttons
        private void corpusDirButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = false;
            folderBrowser.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowser.Description = "Please select the folder whcih contains the audio clips for training";
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                corpusDirTextBox.Text = folderBrowser.SelectedPath;
        }

        private void dictionaryTextBox_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dictionaryFileDialog = new System.Windows.Forms.OpenFileDialog();
            dictionaryFileDialog.Filter = "MAT files (*.mat)|*.mat|All files (*.*)|*.*";
            if (dictionaryFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                dictionaryTextBox.Text = dictionaryFileDialog.FileName;
        }

        private void targetDirButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = false;
            folderBrowser.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowser.Description = "Please select the folder whcih contains the audio clips for encoding";
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                targetDirTextBox.Text = folderBrowser.SelectedPath;
        }

        private void outputDirButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = true;
            folderBrowser.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowser.Description = "Please select the folder for the output audio words";
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                outputDirTextBox.Text = folderBrowser.SelectedPath;
        }

        private void saveManuItem_Click(object sender, RoutedEventArgs e)
        {
            // check and read selected options
            pipelineDoc = new XDocument();
            pipelineDoc = CheckReadSelectedOption();

            // check if piplineDoc is null
            if (pipelineDoc == null)
                return;

            System.Windows.Forms.SaveFileDialog saveFile = new System.Windows.Forms.SaveFileDialog();
            saveFile.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";

            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // save settings
                pipelineDoc.Element("pipeline").Add(new XElement("setting"));
                if (CorpusDir != null)
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("corpusDir", CorpusDir));
                else
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("corpusDir", "null"));

                if (Dictionary != null)
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("dictionary", Dictionary));
                else
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("dictionary", "null"));

                if (TargetDir != null)
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("targetDir", TargetDir));
                else
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("targetDir", "null"));

                if (OutputDir != null)
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("outputDir", OutputDir));
                else
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("outputDir", "null"));

                if (TempDir != System.IO.Path.Combine(System.IO.Path.GetTempPath(), "audio_word_toolbox_temp"))
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("tempDir", TempDir));
                else
                    pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("tempDir", "null"));

                pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("outputExtension", OutputExtension));
                pipelineDoc.Element("pipeline").Element("setting").Add(new XAttribute("fileExistOverwrite", FileExistOverwrite));


                // save the file
                if (pipelineDoc != null)
                {
                    pipelineDoc.Save(saveFile.FileName, SaveOptions.None);
                    System.Windows.MessageBox.Show("Saved.");
                    consoleTextBox.Text += String.Format("Saved: {0}\r\n", saveFile.FileName);
                    consoleTextBox.ScrollToEnd();
                }
            }
        }

        private XDocument CheckReadSelectedOption()
        {
            // initialization
            pipelineDoc = new XDocument();
            pipelineDoc.Add(new XElement("pipeline"));

            // input layer
            pipelineDoc.Element("pipeline").Add(new XElement("inputLayer"));
            Dictionary<string, string> selectedRepresentationOption = ((InputDockPanel)workAreaStackPanel.Children[0]).SelectedRepresentationOption;

            // check if the user complete all the options
            if (!IsOptionDone(pipelineLayerSetup.InputLayer, selectedRepresentationOption))
            {
                System.Windows.MessageBox.Show("Please complete all the options for your input layer.");
                return null;
            }

            // save the settings
            foreach (KeyValuePair<string, string> option in selectedRepresentationOption)
                pipelineDoc.Element("pipeline").Element("inputLayer").Add(new XAttribute(option.Key.Replace(" ", "_"), option.Value));


            // write process
            pipelineDoc.Element("pipeline").Add(new XElement("processes"));
            int numberOfProcess = workAreaStackPanel.Children.Count - 3;
            pipelineDoc.Element("pipeline").Element("processes").Add(new XAttribute("numberOfProcess", Convert.ToString(numberOfProcess)));
            for (int i = 0; i < numberOfProcess; i++)
            {
                string selectedProcessType = ((ProcessDockPanel)workAreaStackPanel.Children[i + 2]).SelectedProcessType;
                Dictionary<string, string> selectedProcessOption = ((ProcessDockPanel)workAreaStackPanel.Children[i + 2]).SelectedProcessOption;

                // check if the user forget to select a process type
                if (selectedProcessType == null)
                {
                    System.Windows.MessageBox.Show(String.Format("Please select a process type for your process at position {0}.", Convert.ToString(i + 1)));
                    return null;
                }

                // start construc the xdocument
                pipelineDoc.Element("pipeline").Element("processes").Add(new XElement("process_" + Convert.ToString(i + 1)));
                pipelineDoc.Element("pipeline").Element("processes").Element("process_" + Convert.ToString(i + 1)).Add(new XAttribute("selectedProcessType", selectedProcessType));
                switch (selectedProcessType)
                {
                    case "Encoding":
                        if (!IsOptionDone(pipelineLayerSetup.EncodingLayer, selectedProcessOption))
                        {
                            System.Windows.MessageBox.Show(String.Format("Please complete all the options for your process at position {0}.", Convert.ToString(i + 1)));
                            return null;
                        }
                        break;
                    case "Pooling":
                        if (!IsOptionDone(pipelineLayerSetup.PoolingLayer, selectedProcessOption))
                        {
                            System.Windows.MessageBox.Show(String.Format("Please complete all the options for your process at position {0}.", Convert.ToString(i + 1)));
                            return null;
                        }
                        break;
                    case "Rectification":
                        if (!IsOptionDone(pipelineLayerSetup.RectificationLayer, selectedProcessOption))
                        {
                            System.Windows.MessageBox.Show(String.Format("Please complete all the options for your process at position {0}.", Convert.ToString(i + 1)));
                            return null;
                        }
                        break;
                    case "Other":
                        if (!IsOptionDone(pipelineLayerSetup.OtherLayer, selectedProcessOption))
                        {
                            System.Windows.MessageBox.Show(String.Format("Please complete all the options for your process at position {0}.", Convert.ToString(i + 1)));
                            return null;
                        }
                        break;
                }

                foreach (KeyValuePair<string, string> option in selectedProcessOption)
                    pipelineDoc.Element("pipeline").Element("processes").Element("process_" + Convert.ToString(i + 1)).Add(new XAttribute(option.Key.Replace(" ", "_"), option.Value));
            }

            return pipelineDoc;
        }

        // Check if the option is not empty
        private bool IsOptionDone(Option layerSetupOption, Dictionary<string, string> selectedProcessOption)
        {
            bool isDone = true;
            if (selectedProcessOption.Count == 0 || !selectedProcessOption.ContainsKey(layerSetupOption.OptionName))
                return false;
            if (layerSetupOption.OptionType == "comboBox")
            {
                if (selectedProcessOption[layerSetupOption.OptionName] == layerSetupOption.OptionName)
                    return false;
                else
                {
                    OptionsSet selectedOptionSet = null;
                    foreach (KeyValuePair<string, OptionsSet> item in ((ComboBoxProperty)layerSetupOption.OptionProperty).Items)
                    {
                        if (selectedProcessOption[layerSetupOption.OptionName] == item.Key)
                        {
                            selectedOptionSet = item.Value;
                            break;
                        }
                    }

                    if (selectedOptionSet == null)
                        return isDone;

                    foreach (Option option in selectedOptionSet.Options)
                    {
                        isDone = IsOptionDone(option, selectedProcessOption);
                        if (!isDone)
                            return false;
                    }
                }
            }
            else
            {
                if (selectedProcessOption[layerSetupOption.OptionName] == "")
                    return false;
            }
            return isDone;
        }

        private void loadManuItem_Click(object sender, RoutedEventArgs e)
        {
            // warn user about discarding current setup
            MessageBoxResult result = System.Windows.MessageBox.Show("Do you want to discard your current setup?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.No)
                return;

            System.Windows.Forms.OpenFileDialog loadfile = new System.Windows.Forms.OpenFileDialog();
            loadfile.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            if (loadfile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // remove all the panel
                while (workAreaStackPanel.Children.Count > 3)
                    workAreaStackPanel.Children.RemoveAt(2);
                workAreaStackPanel.Children.RemoveAt(0);

                // read the XML
                pipelineDoc = XDocument.Load(loadfile.FileName);

                // parse information for input layer
                Dictionary<string, string> selectedRepresentationOption = new Dictionary<string, string>();
                foreach (XAttribute attribute in pipelineDoc.Element("pipeline").Element("inputLayer").Attributes())
                    selectedRepresentationOption.Add(attribute.Name.ToString().Replace("_", " "), attribute.Value);
                InputDockPanel inputDockPanel = new InputDockPanel(pipelineLayerSetup);
                inputDockPanel.SetInputDockPanel(selectedRepresentationOption);
                workAreaStackPanel.Children.Insert(0, inputDockPanel);

                // parse out the number of process
                int numberOfProcess = Convert.ToInt32(pipelineDoc.Element("pipeline").Element("processes").Attribute("numberOfProcess").Value);

                // parse out the information regarding each layer
                for (int i = 0; i < numberOfProcess; i++)
                {
                    string selectedProcessType = "";
                    Dictionary<string, string> selectedProcessOption = new Dictionary<string, string>();
                    foreach (XAttribute attribute in pipelineDoc.Element("pipeline").Element("processes").Element("process_" + Convert.ToString(i + 1)).Attributes())
                    {
                        if (attribute.Name.ToString() == "selectedProcessType")
                            selectedProcessType = attribute.Value;
                        else
                            selectedProcessOption.Add(attribute.Name.ToString().Replace("_", " "), attribute.Value);
                    }

                    // add the process dock panel to work area
                    ProcessDockPanel processDockPanel = new ProcessDockPanel(pipelineLayerSetup);
                    processDockPanel.ProcessDockPanelDeleted += processDockPanel_ProcessDockPanelDeleted;
                    processDockPanel.ProcessDockPanelMovedDown += processDockPanel_ProcessDockPanelMovedDown;
                    processDockPanel.ProcessDockPanelMovedUp += processDockPanel_ProcessDockPanelMovedUp;
                    processDockPanel.SetProcessDockPanel(selectedProcessType, selectedProcessOption);
                    workAreaStackPanel.Children.Insert(2 + i, processDockPanel);
                }

                // read the settings
                string corpusDirTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("corpusDir").Value;
                if (corpusDirTemp != "null")
                    corpusDirTextBox.Text = corpusDirTemp;
                string dictionaryTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("dictionary").Value;
                if (dictionaryTemp != "null")
                    dictionaryTextBox.Text = dictionaryTemp;
                string targetDirTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("targetDir").Value;
                if (targetDirTemp != "null")
                    targetDirTextBox.Text = targetDirTemp;
                string outputDirTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("outputDir").Value;
                if (outputDirTemp != "null")
                    outputDirTextBox.Text = outputDirTemp;
                string tempDirTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("tempDir").Value;
                if (tempDirTemp != "null")
                    TempDir = tempDirTemp;
                string outputExtensionTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("outputExtension").Value;
                OutputExtension = outputExtensionTemp;
                string fileExistOverwriteTemp = pipelineDoc.Element("pipeline").Element("setting").Attribute("fileExistOverwrite").Value;
                if (fileExistOverwriteTemp == "true")
                    FileExistOverwrite = true;
                else
                    FileExistOverwrite = false;

                System.Windows.MessageBox.Show("Loaded.");
                consoleTextBox.Text += String.Format("Loaded: {0}\r\n", loadfile.FileName);
                consoleTextBox.ScrollToEnd();
            }
        }

        // training dcitioanry
        private void EnableAllButton()
        {
            trainButton.IsEnabled = true;
            encodeButton.IsEnabled = true;
            corpusDirButton.IsEnabled = true;
            dictionaryButton.IsEnabled = true;
            targetDirButton.IsEnabled = true;
            outputDirButton.IsEnabled = true;
        }

        private void DisableAllButton()
        {
            trainButton.IsEnabled = false;
            encodeButton.IsEnabled = false;
            corpusDirButton.IsEnabled = false;
            dictionaryButton.IsEnabled = false;
            targetDirButton.IsEnabled = false;
            outputDirButton.IsEnabled = false;
        }

        private void trainButton_Click(object sender, RoutedEventArgs e)
        {
            // disable the button
            DisableAllButton();

            // check/read the input
            if (CorpusDir == null)
            {
                System.Windows.MessageBox.Show("Please input a directory for your dictionary training corpus.");
                EnableAllButton();
                return;
            }
            pipelineDoc = new XDocument();
            pipelineDoc = CheckReadSelectedOption();
            if (pipelineDoc == null)
            {
                EnableAllButton();
                return;
            }

            // begine the training
            // ask the user to define a path for the trained dictioanry
            System.Windows.Forms.SaveFileDialog saveDictionary = new System.Windows.Forms.SaveFileDialog();
            saveDictionary.Filter = "MAT files (*.mat)|*.mat|All files (*.*)|*.*";

            if (saveDictionary.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // re-initialize temp variable
                consoleTextBoxTemp = "";
                dictionaryTextBoxTemp = "";

                // delete exist dictionary
                if (saveDictionary.OverwritePrompt)
                    File.Delete(saveDictionary.FileName);

                // store the input path to temp variable
                dictionaryTextBoxTemp = saveDictionary.FileName;

                BackgroundWorker trainWorker = new BackgroundWorker();
                trainWorker.DoWork += trainWorker_DoWork;
                trainWorker.RunWorkerCompleted += trainWorker_RunWorkerCompleted;
                trainWorker.RunWorkerAsync();
            }
            else
            {
                EnableAllButton();
            }
        }

        private void trainWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            consoleTextBox.Text += consoleTextBoxTemp;
            consoleTextBoxTemp = "";
            if (endWithError)
            {
                consoleTextBox.Text += "Dictionary training finished with ERROR!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Dictionary training finished with ERROR!");
                endWithError = false;
            }
            else if (endWithWarrning)
            {
                consoleTextBox.Text += "Dictionary training finished with WARRNING!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Dictionary training finished with WARRNING!");
                endWithWarrning = false;
            }
            else
            {
                dictionaryTextBox.Text = dictionaryTextBoxTemp;
                dictionaryTextBoxTemp = "";
                consoleTextBox.Text += "Dictionary training finished!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Dictionary training finished!");
            }
            EnableAllButton();
        }

        private void trainWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DictionaryTrain();
        }

        private void DictionaryTrain()
        {
            // process information parsing
            processInformation = ParseProcessInformation();

            // delete the files in temporary directory
            if (Directory.Exists(TempDir))
            {
                foreach (string tempPath in Directory.EnumerateFiles(TempDir, "*.*", SearchOption.AllDirectories))
                    File.Delete(tempPath);
            }

            // convert c# data type to MATLAB data type
            MWCharArray dictionaryMATLAB = (MWCharArray)dictionaryTextBoxTemp;

            // training dictionary for each encoding layer
            string corpusDirTemp = CorpusDir;
            string corpusExtensionTemp = corpusExtension;
            int startIndex = 0;
            List<string> corpusPathesTemp = new List<string>();
            AudioWordEncodeClass audioWordEncodeClass = new AudioWordEncodeClass();
            DictionaryTrainClass dictionaryTrainClass = new DictionaryTrainClass();
            endWithError = false;
            endWithWarrning = false;
            foreach (int encodingLayerIndex in processInformation.encodingLayerIndexList)
            {
                int indexRange = encodingLayerIndex - startIndex;
                List<string> pipelineSublist = processInformation.pipelineList.GetRange(startIndex, indexRange);

                // convert c# data type to MATLAB data type
                MWCellArray pipelineSublistMATLAB = new MWCellArray(pipelineSublist.Count);
                int processIndex = 1;
                foreach (string process in pipelineSublist)
                {
                    pipelineSublistMATLAB[processIndex] = (MWCharArray)process;
                    processIndex++;
                }

                // generate representation for dictionary training
                consoleTextBoxTemp += String.Format("Extracting audio representation for training dictionary at position {0} ... \r\n", Convert.ToString(encodingLayerIndex));
                corpusPathesTemp.Clear();
                AllocConsole();
                foreach (string corpusPath in Directory.EnumerateFiles(corpusDirTemp, corpusExtensionTemp, SearchOption.AllDirectories))
                {
                    string subDir = ((System.IO.Path.GetDirectoryName(corpusPath)).Replace(CorpusDir, "")).Replace(Convert.ToString(System.IO.Path.DirectorySeparatorChar), "");
                    string outDir = System.IO.Path.Combine(TempDir, subDir);
                    consoleTextBoxTemp += String.Format("    Extracting: {0} ... ", corpusPath);
                    MWArray outValue = audioWordEncodeClass.audio_word_encode((MWCharArray)corpusPath, pipelineSublistMATLAB, dictionaryMATLAB, (MWCharArray)outDir, (MWCharArray)"*.mat");
                    string outString = ((MWCharArray)outValue).ToString();
                    if (outString.Substring(0, 6).Equals("ERROR:"))
                    {
                        consoleTextBoxTemp += String.Format("{0}\r\n", outString);
                        endWithError = true;
                        break;
                    }
                    if (outString.Substring(0, 9).Equals("WARRNING:"))
                    {
                        consoleTextBoxTemp += String.Format("{0}\r\n", outString);
                        endWithWarrning = true;
                        continue;
                    }
                    corpusPathesTemp.Add(outString);
                    consoleTextBoxTemp += "done!\r\n";
                }
                FreeConsole();

                if (endWithError)
                    break;

                // add the encoding process into the process subset
                pipelineSublist = processInformation.pipelineList.GetRange(startIndex, indexRange + 1);

                // convert c# data type to MATLAB data type
                MWCellArray corpusPathesMatlab = new MWCellArray(corpusPathesTemp.Count);
                int corpusPathIndex = 1;
                foreach (string corpusPath in corpusPathesTemp)
                {
                    corpusPathesMatlab[corpusPathIndex] = (MWCharArray)corpusPath;
                    corpusPathIndex++;
                }
                pipelineSublistMATLAB = new MWCellArray(pipelineSublist.Count);
                processIndex = 1;
                foreach (string process in pipelineSublist)
                {
                    pipelineSublistMATLAB[processIndex] = (MWCharArray)process;
                    processIndex++;
                }

                // train dictionary
                AllocConsole();
                consoleTextBoxTemp += String.Format("Training dictionary at position {0} ... ", Convert.ToString(encodingLayerIndex));
                MWArray outValueDictionary = dictionaryTrainClass.dictionary_train(corpusPathesMatlab, pipelineSublistMATLAB, dictionaryMATLAB);
                FreeConsole();

                // check the training result
                string outStringDictionary = ((MWCharArray)outValueDictionary).ToString();
                if (outStringDictionary.Substring(0, 6).Equals("ERROR:"))
                {
                    consoleTextBoxTemp += String.Format("{0}\r\n", outStringDictionary);
                    endWithError = true;
                    break;
                }
                else
                {
                    consoleTextBoxTemp += "done!\r\n";
                }

                // setting the start index for the next codebook
                startIndex = encodingLayerIndex;

                // setting the training directory and extension for the next dictionary
                corpusDirTemp = TempDir;
                corpusExtensionTemp = "*.mat";
            }

            // delete extracted files in the temp file
            consoleTextBoxTemp += "Delete the files in the temporary folder ... ";
            foreach (string corpusPath in corpusPathesTemp)
                File.Delete(corpusPath);
            consoleTextBoxTemp += "done!\r\n";
        }

        // encode examples
        private void encodeButton_Click(object sender, RoutedEventArgs e)
        {
            // disable the button
            DisableAllButton();

            // process information parsing
            pipelineDoc = new XDocument();
            pipelineDoc = CheckReadSelectedOption();
            if (pipelineDoc == null)
            {
                EnableAllButton();
                return;
            }
            processInformation = ParseProcessInformation();

            // check the input
            if (TargetDir == null)
            {
                System.Windows.MessageBox.Show("Please provide the path to the directory which contains your target audio clips.");
                EnableAllButton();
                return;
            }
            if (OutputDir == null)
            {
                System.Windows.MessageBox.Show("Please provide the path to your desired directory for your output audio clips.");
                EnableAllButton();
                return;
            }
            if (processInformation.encodingLayerIndexList.Count > 0 && Dictionary == null)
            {
                System.Windows.MessageBox.Show("Please provide the path to your desired dictionary.");
                EnableAllButton();
                return;
            }

            // begine the extraction
            BackgroundWorker encodeWorker = new BackgroundWorker();
            encodeWorker.DoWork += encodeWorker_DoWork;
            encodeWorker.RunWorkerCompleted += encodeWorker_RunWorkerCompleted;
            encodeWorker.RunWorkerAsync();
        }

        private void encodeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            consoleTextBox.Text += consoleTextBoxTemp;
            consoleTextBoxTemp = "";
            if (endWithError)
            {
                consoleTextBox.Text += "Audio word extraction finished with ERROR!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Audio word extraction finished with ERROR!");
                endWithError = false;
            }
            else if (endWithWarrning)
            {
                consoleTextBox.Text += "Audio word extraction finished with WARRNING!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Audio word extraction finished with WARRNING!");
                endWithWarrning = false;
            }
            else
            {
                consoleTextBox.Text += "Audio word extraction finished!\r\n";
                consoleTextBox.ScrollToEnd();
                System.Windows.MessageBox.Show("Audio word extraction finished!");
            }
            EnableAllButton();
        }

        private void encodeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            AudioWordEncode();
        }

        private void AudioWordEncode()
        {
            // re-initialize temp variable
            consoleTextBoxTemp = "";

            // convert c# data type to MATLAB data type
            MWCharArray dictionaryMATLAB = (MWCharArray)Dictionary;
            MWCellArray pipelineListMATLAB = new MWCellArray(processInformation.pipelineList.Count);
            int processIndex = 1;
            foreach (string process in processInformation.pipelineList)
            {
                pipelineListMATLAB[processIndex] = (MWCharArray)process;
                processIndex++;
            }

            // list and shuffle targetPathes
            IEnumerable<string> targetPathes = Directory.EnumerateFiles(TargetDir, targetExtension, SearchOption.AllDirectories);
            Random random = new Random();
            targetPathes = targetPathes.OrderBy(order => random.Next()).ToList();

            // audio word extraction
            AudioWordEncodeClass audioWordEncodeClass = new AudioWordEncodeClass();
            consoleTextBoxTemp += "Extracting audio word representation foreach audio clips in the input directory ... \r\n";
            AllocConsole();
            foreach (string targetPath in targetPathes)
            {
                // construct all the pathes
                string subDir = ((System.IO.Path.GetDirectoryName(targetPath)).Replace(TargetDir, "")).Replace(Convert.ToString(System.IO.Path.DirectorySeparatorChar), "");
                string outDir = System.IO.Path.Combine(OutputDir, subDir);
                string filename = System.IO.Path.GetFileNameWithoutExtension(targetPath);
                string outPath = System.IO.Path.Combine(outDir, filename + OutputExtension.Substring(1));
                consoleTextBoxTemp += String.Format("    Extracting: {0} ... ", targetPath);

                // check if the output file has already exist
                if (File.Exists(outPath))
                {
                    if (FileExistOverwrite)
                    {
                        File.Delete(outPath);
                    }
                    else
                    {
                        consoleTextBoxTemp += "skipped!\r\n";
                        continue;
                    }
                }

                // start audio word extraction
                MWArray outValue = audioWordEncodeClass.audio_word_encode((MWCharArray)targetPath, pipelineListMATLAB, dictionaryMATLAB, (MWCharArray)outDir, (MWCharArray)OutputExtension);
                string outString = ((MWCharArray)outValue).ToString();
                if (outString.Substring(0, 6).Equals("ERROR:"))
                {
                    consoleTextBoxTemp += String.Format("{0}\r\n", outString);
                    endWithError = true;
                    break;
                }
                if (outString.Substring(0, 9).Equals("WARRNING:"))
                {
                    consoleTextBoxTemp += String.Format("done with {0}\r\n", outString);
                    endWithWarrning = true;
                    continue;
                }
                consoleTextBoxTemp += "done!\r\n";
            }
            FreeConsole();
        }

        // parse the process information out of the UI
        private ProcessInformation ParseProcessInformation()
        {
            // initialize for information parsing
            ProcessInformation processInformation = new ProcessInformation();
            processInformation.pipelineList = new List<string>();
            processInformation.encodingLayerIndexList = new List<int>();

            // parse information for input layer
            processInformation.pipelineList.Add("Input;");
            foreach (XAttribute attribute in pipelineDoc.Element("pipeline").Element("inputLayer").Attributes())
                processInformation.pipelineList[0] += (attribute.Name.ToString().Replace("_", " ") + "=" + attribute.Value + ";");

            // parse out the number of process
            int numberOfProcess = Convert.ToInt32(pipelineDoc.Element("pipeline").Element("processes").Attribute("numberOfProcess").Value);

            // parse out the information regarding each layer and identify encoding process
            for (int i = 0; i < numberOfProcess; i++)
            {
                foreach (XAttribute attribute in pipelineDoc.Element("pipeline").Element("processes").Element("process_" + Convert.ToString(i + 1)).Attributes())
                {
                    if (attribute.Name.ToString() == "selectedProcessType")
                        processInformation.pipelineList.Add(attribute.Value + ";");
                    else
                        processInformation.pipelineList[i + 1] += (attribute.Name.ToString().Replace("_", " ") + "=" + attribute.Value + ";");
                    if (attribute.Value == "Encoding")
                        processInformation.encodingLayerIndexList.Add(i + 1);
                }
            }

            // return the process information
            return processInformation;
        }

        // class that stores the process information
        private class ProcessInformation
        {
            public List<string> pipelineList { get; set; }
            public List<int> encodingLayerIndexList { get; set; }
        }


        // the following two functions are for open/close console
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();


        // settings
        private void outputExtensionMATManuItem_Click(object sender, RoutedEventArgs e)
        {
            OutputExtension = "*.mat";
        }

        private void outputExtensionCSVManuItem_Click(object sender, RoutedEventArgs e)
        {
            OutputExtension = "*.csv";
        }

        private void fileExistActionOverwriteManuItem_Click(object sender, RoutedEventArgs e)
        {
            FileExistOverwrite = true;
        }

        private void fileExistActionSkipManuItem_Click(object sender, RoutedEventArgs e)
        {
            FileExistOverwrite = false;
        }

        private void tempDirManuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = true;
            folderBrowser.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowser.Description = "Please select the folder for saving temporary files while dictionary training";
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                TempDir = folderBrowser.SelectedPath;
        }

        private void aboutManuItem_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.ShowDialog();
        }
    }
}