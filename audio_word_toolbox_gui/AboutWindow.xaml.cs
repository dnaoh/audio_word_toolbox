﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Diagnostics;
using System.Windows.Media.Animation;

namespace audio_word_toolbox
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        int numberOfClick = 0;

        public AboutWindow()
        {
            InitializeComponent();
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            versionRun.Text = "Version " + version.Major.ToString() + "." + version.Minor.ToString();
        }

        private void audioWordIconImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            numberOfClick++;
            if (numberOfClick >= 10)
            {
                Uri syHeadUri = new Uri(@"/audio_word_toolbox;component/sy_head.png", UriKind.Relative);
                audioWordIconImage.Source = new BitmapImage(syHeadUri);
                AnimateHead(audioWordIconImage, 1, 1);

                // unsubscribe this event
                audioWordIconImage.MouseDown -= audioWordIconImage_MouseDown;
            }
        }

        private void AnimateHead(Image haeadImage, double scaleTime, double rotateTime)
        {
            // scaling animation
            DoubleAnimation scaleDoubleAnimation = new DoubleAnimation();
            scaleDoubleAnimation.From = 100;
            scaleDoubleAnimation.To = 200;
            scaleDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(scaleTime));
            haeadImage.BeginAnimation(Image.HeightProperty, scaleDoubleAnimation);

            // rotating animation
            DoubleAnimation rotateDoubleAnimation = new DoubleAnimation();
            rotateDoubleAnimation.From = 0;
            rotateDoubleAnimation.To = 360;
            rotateDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(rotateTime));
            rotateDoubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            RotateTransform rotateTransform = new RotateTransform();
            rotateTransform.CenterX = 100;
            rotateTransform.CenterY = 100;
            haeadImage.RenderTransform = rotateTransform;
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateDoubleAnimation);
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
