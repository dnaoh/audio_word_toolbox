﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;

namespace audio_word_toolbox
{
    class LayerSetup
    {
        public Option InputLayer { get; private set; }
        public Option EncodingLayer { get; private set; }
        public Option PoolingLayer { get; private set; }
        public Option RectificationLayer { get; private set; }
        public Option OtherLayer { get; private set; }

        public LayerSetup()
        {
            XDocument pipelineLayertSetupDoc = XDocument.Parse(audio_word_toolbox.Properties.Resources.LayerSetup);
            InputLayer = new Option(pipelineLayertSetupDoc.Root.Element("InputLayer"));
            EncodingLayer = new Option(pipelineLayertSetupDoc.Root.Element("EncodingLayer"));
            PoolingLayer = new Option(pipelineLayertSetupDoc.Root.Element("PoolingLayer"));
            RectificationLayer = new Option(pipelineLayertSetupDoc.Root.Element("RectificationLayer"));
            OtherLayer = new Option(pipelineLayertSetupDoc.Root.Element("OtherLayer"));
        }
    }

    // option class contain a options pertain to a given process
    class Option
    {
        public string OptionName { get; private set; }
        public string OptionType { get; private set; }
        public Property OptionProperty { get; private set; }

        public Option(XElement optionElement)
        {
            OptionName = optionElement.Attribute("optionName").Value;
            OptionType = optionElement.Attribute("optionType").Value;

            switch (OptionType)
            {
                case "integerUpDown":
                    OptionProperty = new IntegerUpDownProperty(optionElement);
                    break;
                case "doubleUpDown":
                    OptionProperty = new DoubleUpDownProperty(optionElement);
                    break;
                case "comboBox":
                    OptionProperty = new ComboBoxProperty(optionElement);
                    break;
            }
        }
    }

    // option property class for holding the properties for a given option
    class Property { }

    class IntegerUpDownProperty : Property
    {
        public string Name { get; private set; }
        public string Watermark { get; private set; }
        public int Maximum { get; private set; }
        public int Minimum { get; private set; }

        public IntegerUpDownProperty(XElement optionElement)
        {
            Name = optionElement.Attribute("optionName").Value;
            Watermark = optionElement.Attribute("watermark").Value;

            if (optionElement.Attribute("maximum") != null)
                Maximum = Convert.ToInt32(optionElement.Attribute("maximum").Value);
            else
                Maximum = int.MaxValue;

            if (optionElement.Attribute("minimum") != null)
                Minimum = Convert.ToInt32(optionElement.Attribute("minimum").Value);
            else
                Minimum = int.MinValue;
        }
    }

    class DoubleUpDownProperty : Property
    {
        public string Name { get; private set; }
        public string Watermark { get; private set; }
        public double Maximum { get; private set; }
        public double Minimum { get; private set; }
        public double Increment { get; private set; }

        public DoubleUpDownProperty(XElement optionElement)
        {
            Name = optionElement.Attribute("optionName").Value;
            Watermark = optionElement.Attribute("watermark").Value;

            if (optionElement.Attribute("increment") != null)
                Increment = Convert.ToDouble(optionElement.Attribute("increment").Value);
            else
                Increment = 0.01;

            if (optionElement.Attribute("maximum") != null)
                Maximum = Convert.ToDouble(optionElement.Attribute("maximum").Value);
            else
                Maximum = double.MaxValue;

            if (optionElement.Attribute("minimum") != null)
                Minimum = Convert.ToDouble(optionElement.Attribute("minimum").Value);
            else
                Minimum = double.MinValue;
        }
    }

    class ComboBoxProperty : Property
    {
        public string Name { get; private set; }
        public int NumberOfItem { get; private set; }
        public Dictionary<string, OptionsSet> Items { get; private set; }

        public ComboBoxProperty(XElement optionElement)
        {
            Name = optionElement.Attribute("optionName").Value;
            NumberOfItem = Convert.ToInt32(optionElement.Attribute("numberOfItem").Value);
            Items = new Dictionary<string, OptionsSet>();

            foreach (XElement itemElement in optionElement.Elements())
            {
                if (itemElement.Attribute("numberOfOption") != null)
                    Items.Add(itemElement.Attribute("itemName").Value, new OptionsSet(itemElement));
                else
                    Items.Add(itemElement.Attribute("itemName").Value, null);
            }
        }
    }

    // options class contain all the options pertain to a given process
    class OptionsSet
    {
        public int NumberOfOption { get; private set; }
        public List<Option> Options { get; private set; }

        public OptionsSet(XElement processElement)
        {
            NumberOfOption = Convert.ToInt32(processElement.Attribute("numberOfOption").Value);
            Options = new List<Option>();
            foreach (XElement optionElement in processElement.Elements())
            {
                Options.Add(new Option(optionElement));
            }
        }
    }
}