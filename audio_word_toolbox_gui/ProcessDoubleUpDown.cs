﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using Xceed.Wpf.Toolkit;

namespace audio_word_toolbox
{
    class ProcessDoubleUpDown : DoubleUpDown
    {
        public DoubleUpDownProperty OptionProperty { private set; get; }

        public ProcessDoubleUpDown(DoubleUpDownProperty doubleUpDownProperty)
        {
            Height = 22;
            Width = 60;
            Margin = new Thickness(10, 0, 0, 0);
            ToolTip = doubleUpDownProperty.Name;
            Watermark = doubleUpDownProperty.Watermark;
            Maximum = doubleUpDownProperty.Maximum;
            Minimum = doubleUpDownProperty.Minimum;
            Increment = doubleUpDownProperty.Increment;
            OptionProperty = doubleUpDownProperty;
        }
    }
}
