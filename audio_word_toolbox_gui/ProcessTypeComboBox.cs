﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;

namespace audio_word_toolbox
{
    class ProcessTypeComboBox : ComboBox
    {
        public ProcessTypeComboBox()
        {
            // Initialize a combo box with the following setting
            IsReadOnly = true;
            VerticalAlignment = System.Windows.VerticalAlignment.Center;
            SelectedIndex = 0;
            Margin = new Thickness(10, 0, 0, 0);
            SelectedValuePath = "Content";


            // Add the input list as the Cotent for the combo box
            Items.Add(new ComboBoxItem() { Content = "Process Type" });
            Items.Add(new ComboBoxItem() { Content = "Encoding" });
            Items.Add(new ComboBoxItem() { Content = "Pooling" });
            Items.Add(new ComboBoxItem() { Content = "Rectification" });
            Items.Add(new ComboBoxItem() { Content = "Other" });
        }
    }
}