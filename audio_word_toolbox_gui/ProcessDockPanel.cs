﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace audio_word_toolbox
{
    class ProcessDockPanel : DockPanel
    {
        // property for the class
        private LayerSetup PipelineLayerSetup { get; set; }
        public string SelectedProcessType { private set; get; }
        private Dictionary<string, string> selectedProcessOption = new Dictionary<string, string>();
        public Dictionary<string, string> SelectedProcessOption
        {
            get
            {
                selectedProcessOption.Clear();
                for (int i = 0; i < this.Children.Count; i++)
                {
                    if (this.Children[i] is ProcessIntegerUpDown)
                    {
                        selectedProcessOption.Add(((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name, Convert.ToString(((ProcessIntegerUpDown)this.Children[i]).Value));
                    }
                    if (this.Children[i] is ProcessDoubleUpDown)
                    {
                        selectedProcessOption.Add(((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name, Convert.ToString(((ProcessDoubleUpDown)this.Children[i]).Value));
                    }
                    if (this.Children[i] is ProcessComboBox)
                    {
                        selectedProcessOption.Add(((ProcessComboBox)this.Children[i]).OptionProperty.Name, ((ProcessComboBox)this.Children[i]).Text);
                    }
                }
                return selectedProcessOption;
            }
        }

        public ProcessDockPanel(LayerSetup pipelineLayerSetup)
        {
            // set the property of the stack panel
            VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Height = 42;
            Background = new SolidColorBrush(Colors.Gray);
            Margin = new Thickness(2, 1, 2, 1);
            LastChildFill = false;
            PipelineLayerSetup = pipelineLayerSetup;

            // add the combo box for process type into the dock panel
            ProcessTypeComboBox processTypeComboBox = new ProcessTypeComboBox();
            processTypeComboBox.SelectionChanged += processTypeComboBox_SelectionChanged;
            DockPanel.SetDock(processTypeComboBox, Dock.Left);
            Children.Add(processTypeComboBox);

            // add the delete button at the far left
            Button deleteButton = new Button()
            {
                Content = "Delete",
                Width = 50,
                Margin = new Thickness(0, 0, 10, 0),
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
            };
            deleteButton.Click += deleteButton_Click;
            DockPanel.SetDock(deleteButton, Dock.Right);
            Children.Add(deleteButton);

            // add the move down button next to the delete button
            Button moveDownButton = new Button()
            {
                Content = "↓",
                Width = 25,
                Margin = new Thickness(0, 0, 10, 0),
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
            };
            moveDownButton.Click += moveDownButton_Click;
            DockPanel.SetDock(moveDownButton, Dock.Right);
            Children.Add(moveDownButton);

            // add the move up button next to the move down button
            Button moveUpButton = new Button()
            {
                Content = "↑",
                Width = 25,
                Margin = new Thickness(0, 0, 10, 0),
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
            };
            moveUpButton.Click += moveUpButton_Click;
            DockPanel.SetDock(moveUpButton, Dock.Right);
            Children.Add(moveUpButton);
        }

        // process type
        private void processTypeComboBox_SelectionChanged(object sender, EventArgs e)
        {
            // remove pre-exist children in the dock panel
            if (this.Children.Count > 4)
                this.Children.RemoveRange(4, this.Children.Count - 4);

            SelectedProcessType = ((ProcessTypeComboBox)sender).SelectedValue.ToString();

            // action for selection different types
            switch (SelectedProcessType)
            {
                case "Encoding":
                    // set background
                    this.Background = new SolidColorBrush(Colors.LightGreen);
                    AddControl(PipelineLayerSetup.EncodingLayer);

                    break;
                case "Pooling":
                    // set background
                    this.Background = new SolidColorBrush(Colors.LightBlue);
                    AddControl(PipelineLayerSetup.PoolingLayer);

                    break;
                case "Rectification":
                    // set background
                    this.Background = new SolidColorBrush(Colors.Gold);
                    AddControl(PipelineLayerSetup.RectificationLayer);

                    break;
                case "Other":
                    // set background
                    this.Background = new SolidColorBrush(Colors.LightPink);
                    AddControl(PipelineLayerSetup.OtherLayer);

                    break;
                default:
                    // set background
                    this.Background = new SolidColorBrush(Colors.Gray);

                    break;
            }
        }

        // add new UI controls based on the input option
        private void AddControl(Option option)
        {
            switch (option.OptionType)
            {
                case "integerUpDown":
                    ProcessIntegerUpDown processIntegerUpDown = new ProcessIntegerUpDown((IntegerUpDownProperty)option.OptionProperty);
                    DockPanel.SetDock(processIntegerUpDown, Dock.Left);
                    Children.Add(processIntegerUpDown);

                    break;
                case "doubleUpDown":
                    ProcessDoubleUpDown processDoubleUpDown = new ProcessDoubleUpDown((DoubleUpDownProperty)option.OptionProperty);
                    DockPanel.SetDock(processDoubleUpDown, Dock.Left);
                    Children.Add(processDoubleUpDown);

                    break;
                case "comboBox":
                    ProcessComboBox processComboBox = new ProcessComboBox((ComboBoxProperty)option.OptionProperty, Children.Count + 1);
                    DockPanel.SetDock(processComboBox, Dock.Left);
                    processComboBox.SelectionChanged += processComboBox_SelectionChanged;
                    Children.Add(processComboBox);

                    break;
            }
        }

        private void processComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // delete any control after this
            if (((ProcessComboBox)sender).NumberOfAssociateOptions != 0)
            {
                if (Children.Count > ((ProcessComboBox)sender).IndexInProcessPanel)
                    Children.RemoveRange(((ProcessComboBox)sender).IndexInProcessPanel, Children.Count);
            }

            // figure out which item is selected and extract the further options
            OptionsSet selectedOptionSet = null;
            foreach (KeyValuePair<string, OptionsSet> item in ((ProcessComboBox)sender).OptionProperty.Items)
            {
                if (((ProcessComboBox)sender).SelectedValue.ToString() == item.Key)
                {
                    selectedOptionSet = item.Value;
                    break;
                }
            }

            // check if it has further options
            if (selectedOptionSet == null)
                return;

            ((ProcessComboBox)sender).NumberOfAssociateOptions = selectedOptionSet.NumberOfOption;
            foreach (Option option in selectedOptionSet.Options)
                AddControl(option);
        }

        public void SetProcessDockPanel(string selectedProcessType, Dictionary<string, string> selectedProcessOption)
        {
            ((ProcessTypeComboBox)this.Children[0]).Text = selectedProcessType;
            SetControl(selectedProcessOption);
        }

        private void SetControl(Dictionary<string, string> selectedProcessOption)
        {
            for (int i = 1; i < this.Children.Count; i++)
            {
                if (this.Children[i] is ProcessIntegerUpDown)
                {
                    if (((ProcessIntegerUpDown)this.Children[i]).Value != null)
                        continue;
                    ((ProcessIntegerUpDown)this.Children[i]).Value = Convert.ToInt32(selectedProcessOption[((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name]);
                    selectedProcessOption.Remove(((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name);
                    break;
                }
                if (this.Children[i] is ProcessDoubleUpDown)
                {
                    if (((ProcessDoubleUpDown)this.Children[i]).Value != null)
                        continue;
                    ((ProcessDoubleUpDown)this.Children[i]).Value = Convert.ToDouble(selectedProcessOption[((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name]);
                    selectedProcessOption.Remove(((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name);
                    break;
                }
                if (this.Children[i] is ProcessComboBox)
                {
                    if (((ProcessComboBox)this.Children[i]).Text != ((ProcessComboBox)this.Children[i]).OptionProperty.Name)
                        continue;
                    ((ProcessComboBox)this.Children[i]).Text = selectedProcessOption[((ProcessComboBox)this.Children[i]).OptionProperty.Name];
                    selectedProcessOption.Remove(((ProcessComboBox)this.Children[i]).OptionProperty.Name);
                    break;
                }
            }

            if (selectedProcessOption.Count > 0)
                SetControl(selectedProcessOption);
        }

        //// The following code is for Delete/Move Down/Move Up
        // property for deletion
        private bool isDeleted;
        public bool IsDeleted
        {
            get
            {
                return isDeleted;
            }
            set
            {
                isDeleted = value;
                OnProcessDockPanelDeleted(IsDeleted);
            }
        }

        // property for moveing down
        private bool isMovingDown;
        public bool IsMovingDown
        {
            get
            {
                return isMovingDown;
            }
            set
            {
                isMovingDown = value;
                OnProcessDockPanelMovedDown(IsMovingDown);
            }
        }

        // property for moveing up
        private bool isMovingUp;
        public bool IsMovingUp
        {
            get
            {
                return isMovingUp;
            }
            set
            {
                isMovingUp = value;
                OnProcessDockPanelMovedUp(IsMovingUp);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            IsDeleted = true;
        }

        private void moveDownButton_Click(object sender, RoutedEventArgs e)
        {
            IsMovingDown = true;
        }

        private void moveUpButton_Click(object sender, RoutedEventArgs e)
        {
            IsMovingUp = true;
        }

        // event hadler for deletion
        public event EventHandler ProcessDockPanelDeleted;
        public void OnProcessDockPanelDeleted(bool isDeleted)
        {
            if (ProcessDockPanelDeleted != null)
            {
                DeleteEventArgs deleteEventArgs = new DeleteEventArgs(isDeleted);
                ProcessDockPanelDeleted(this, deleteEventArgs);
            }
        }

        // event handler for move the dock panel down
        public event EventHandler ProcessDockPanelMovedDown;
        public void OnProcessDockPanelMovedDown(bool isMovingDown)
        {
            if (ProcessDockPanelMovedDown != null)
            {
                MoveDownEventArgs moveDownEventArgs = new MoveDownEventArgs(isMovingDown);
                ProcessDockPanelMovedDown(this, moveDownEventArgs);
            }
        }

        // event handler for move the dock panel up
        public event EventHandler ProcessDockPanelMovedUp;
        public void OnProcessDockPanelMovedUp(bool isMovingUp)
        {
            if (ProcessDockPanelMovedUp != null)
            {
                MoveUpEventArgs moveUpEventArgs = new MoveUpEventArgs(isMovingUp);
                ProcessDockPanelMovedUp(this, moveUpEventArgs);
            }
        }
    }

    class DeleteEventArgs : EventArgs
    {
        public bool IsDeleted { get; private set; }
        public DeleteEventArgs(bool isDeleted)
        {
            IsDeleted = isDeleted;
        }
    }

    class MoveDownEventArgs : EventArgs
    {
        public bool IsMovingDown { get; private set; }
        public MoveDownEventArgs(bool isMovingDown)
        {
            IsMovingDown = isMovingDown;
        }
    }

    class MoveUpEventArgs : EventArgs
    {
        public bool IsMovingUp { get; private set; }
        public MoveUpEventArgs(bool isMovingUp)
        {
            IsMovingUp = isMovingUp;
        }
    }
}