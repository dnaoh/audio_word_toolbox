﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace audio_word_toolbox
{
    class InputDockPanel : DockPanel
    {
        // property for the class
        private LayerSetup PipelineLayerSetup { get; set; }
        private Dictionary<string, string> selectedRepresentationOption = new Dictionary<string, string>();
        public Dictionary<string, string> SelectedRepresentationOption
        {
            get
            {
                selectedRepresentationOption.Clear();
                for (int i = 0; i < this.Children.Count; i++)
                {
                    if (this.Children[i] is ProcessIntegerUpDown)
                    {
                        selectedRepresentationOption.Add(((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name, Convert.ToString(((ProcessIntegerUpDown)this.Children[i]).Value));
                    }
                    if (this.Children[i] is ProcessDoubleUpDown)
                    {
                        selectedRepresentationOption.Add(((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name, Convert.ToString(((ProcessDoubleUpDown)this.Children[i]).Value));
                    }
                    if (this.Children[i] is ProcessComboBox)
                    {
                        selectedRepresentationOption.Add(((ProcessComboBox)this.Children[i]).OptionProperty.Name, ((ProcessComboBox)this.Children[i]).Text);
                    }
                }
                return selectedRepresentationOption;
            }
        }

        public InputDockPanel(LayerSetup pipelineLayerSetup)
        {
            // set the property of the stack panel
            VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Height = 42;
            Background = new SolidColorBrush(Colors.Black);
            Margin = new Thickness(2, 1, 2, 1);
            LastChildFill = false;
            PipelineLayerSetup = pipelineLayerSetup;

            // add new UI controls
            AddControl(PipelineLayerSetup.InputLayer);
        }

        // add new UI controls based on the input option
        private void AddControl(Option option)
        {
            switch (option.OptionType)
            {
                case "integerUpDown":
                    ProcessIntegerUpDown processIntegerUpDown = new ProcessIntegerUpDown((IntegerUpDownProperty)option.OptionProperty);
                    DockPanel.SetDock(processIntegerUpDown, Dock.Left);
                    Children.Add(processIntegerUpDown);

                    break;
                case "doubleUpDown":
                    ProcessDoubleUpDown processDoubleUpDown = new ProcessDoubleUpDown((DoubleUpDownProperty)option.OptionProperty);
                    DockPanel.SetDock(processDoubleUpDown, Dock.Left);
                    Children.Add(processDoubleUpDown);

                    break;
                case "comboBox":
                    ProcessComboBox processComboBox = new ProcessComboBox((ComboBoxProperty)option.OptionProperty, Children.Count + 1);
                    DockPanel.SetDock(processComboBox, Dock.Left);
                    processComboBox.SelectionChanged += processComboBox_SelectionChanged;
                    Children.Add(processComboBox);

                    break;
            }
        }

        private void processComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // delete any control after this
            if (Children.Count > ((ProcessComboBox)sender).IndexInProcessPanel)
                Children.RemoveRange(((ProcessComboBox)sender).IndexInProcessPanel, ((ProcessComboBox)sender).NumberOfAssociateOptions);

            // figure out which item is selected and extract the further options
            OptionsSet selectedOptionSet = null;
            foreach (KeyValuePair<string, OptionsSet> item in ((ProcessComboBox)sender).OptionProperty.Items)
            {
                if (((ProcessComboBox)sender).SelectedValue.ToString() == item.Key)
                {
                    selectedOptionSet = item.Value;
                    break;
                }
            }

            // check if it has further options
            if (selectedOptionSet == null)
                return;

            ((ProcessComboBox)sender).NumberOfAssociateOptions = selectedOptionSet.NumberOfOption;
            foreach (Option option in selectedOptionSet.Options)
                AddControl(option);
        }

        public void SetInputDockPanel(Dictionary<string, string> selectedRepresentationOption)
        {
            for (int i = 0; i < this.Children.Count; i++)
            {
                if (this.Children[i] is ProcessIntegerUpDown)
                {
                    if (((ProcessIntegerUpDown)this.Children[i]).Value != null)
                        continue;
                    ((ProcessIntegerUpDown)this.Children[i]).Value = Convert.ToInt32(selectedRepresentationOption[((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name]);
                    selectedRepresentationOption.Remove(((ProcessIntegerUpDown)this.Children[i]).OptionProperty.Name);
                    break;
                }
                if (this.Children[i] is ProcessDoubleUpDown)
                {
                    if (((ProcessIntegerUpDown)this.Children[i]).Value != null)
                        continue;
                    ((ProcessDoubleUpDown)this.Children[i]).Value = Convert.ToDouble(selectedRepresentationOption[((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name]);
                    selectedRepresentationOption.Remove(((ProcessDoubleUpDown)this.Children[i]).OptionProperty.Name);
                    break;
                }
                if (this.Children[i] is ProcessComboBox)
                {
                    if (((ProcessComboBox)this.Children[i]).Text != ((ProcessComboBox)this.Children[i]).OptionProperty.Name)
                        continue;
                    ((ProcessComboBox)this.Children[i]).Text = selectedRepresentationOption[((ProcessComboBox)this.Children[i]).OptionProperty.Name];
                    selectedRepresentationOption.Remove(((ProcessComboBox)this.Children[i]).OptionProperty.Name);
                    break;
                }
            }

            if (selectedRepresentationOption.Count > 0)
                SetInputDockPanel(selectedRepresentationOption);
        }
    }
}