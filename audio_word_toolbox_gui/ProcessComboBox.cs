﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;

namespace audio_word_toolbox
{
    class ProcessComboBox : ComboBox
    {
        public ComboBoxProperty OptionProperty { private set; get; }
        public int IndexInProcessPanel { private set; get; }
        public int NumberOfAssociateOptions { set; get; }

        public ProcessComboBox(ComboBoxProperty comboBoxProperty, int indexInProcessPanel)
        {
            // Initialize a combo box with the following setting
            IsReadOnly = true;
            VerticalAlignment = System.Windows.VerticalAlignment.Center;
            SelectedIndex = 0;
            Margin = new Thickness(10, 0, 0, 0);
            SelectedValuePath = "Content";
            Items.Add(new ComboBoxItem() { Content = comboBoxProperty.Name });
            OptionProperty = comboBoxProperty;
            IndexInProcessPanel = indexInProcessPanel;
            NumberOfAssociateOptions = 0;

            // Add the input list as the Cotent for the combo box
            for (int i = 0; i < OptionProperty.NumberOfItem; i++)
            {
                Items.Add(new ComboBoxItem() { Content = OptionProperty.Items.Keys.ElementAt(i) });
            }
        }
    }
}