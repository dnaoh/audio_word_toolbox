clear
clc

addpath('audio_word_encode');
addpath('dictionary_train');

test_input_dir = 'C:\Users\USER\Desktop\test\_wav';
test_temp_dir = 'C:\Users\USER\Desktop\test\temp_triangle_vector';
test_out_dir_triangle = 'C:\Users\USER\Desktop\test\fina_triangle';
test_out_dir_vector = 'C:\Users\USER\Desktop\test\fina_vector';
test_out_dir_sparse = 'C:\Users\USER\Desktop\test\fina_sparse';

test_pathes = listfile(test_input_dir, '.wav');
test_dic_triangle = 'C:\Users\USER\Desktop\test\test_dic_triangle.mat';
test_dic_vector = 'C:\Users\USER\Desktop\test\test_dic_vector.mat';
test_dic_sparse = 'C:\Users\USER\Desktop\test\test_dic_sparse.mat';

if exist(test_dic_triangle, 'file')
    delete(test_dic_triangle);
end
if exist(test_dic_vector, 'file')
    delete(test_dic_vector);
end
if exist(test_dic_sparse, 'file')
    delete(test_dic_sparse);
end
if exist(test_temp_dir, 'dir')
    rmdir(test_temp_dir,'s');
end
if exist(test_out_dir_triangle, 'dir')
    rmdir(test_out_dir_triangle,'s');
end
if exist(test_out_dir_vector, 'dir')
    rmdir(test_out_dir_vector,'s');
end
if exist(test_out_dir_sparse, 'dir')
    rmdir(test_out_dir_sparse,'s');
end

% extract feature for
test_pathes_out = cell(size(test_pathes));
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    };
for i = 1:length(test_pathes)
    test_path = test_pathes{i};
    test_pathes_out{i} = audio_word_encode(test_path, processes, test_dic_triangle, test_temp_dir);
end

%% triangle coding
% test dictionary training #1 for triangle coding
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Triangle Coding;Dictionary=Random Samples;Dictionary Size=1024;';
    };
test_dic_triangle = dictionary_train(test_pathes_out, processes, test_dic_triangle);

% final aw extraction for triangle
final_aw_triangle = audio_word_encode(test_pathes{1}, processes, test_dic_triangle, test_out_dir_triangle);

%% vector quantization
% test dictionary training #1 for vector quantization
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Vector Quantization;Dictionary=Random Samples;Dictionary Size=1024;';
    };
test_dic_vector = dictionary_train(test_pathes_out, processes, test_dic_vector);

% final aw extraction for vector quantization
final_aw_vector = audio_word_encode(test_pathes{1}, processes, test_dic_vector, test_out_dir_vector);

%% sparse coding
% test dictionary training #1 for sparse coding
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=1024;';
    };
test_dic_sparse = dictionary_train(test_pathes_out, processes, test_dic_sparse);

% final aw extraction for vector quantization
final_aw_sparse = audio_word_encode(test_pathes{1}, processes, test_dic_sparse, test_out_dir_sparse);