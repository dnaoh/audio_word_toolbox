clear
clc

addpath('audio_word_encode');
addpath('dictionary_train');

test_input_dir = 'C:\Users\USER\Desktop\test\_wav';
test_out_dir_time_series = 'C:\Users\USER\Desktop\test\fina_time_series';
test_out_dir_spectrum = 'C:\Users\USER\Desktop\test\fina_spectrum';
test_out_dir_mel_spectrum = 'C:\Users\USER\Desktop\test\fina_mel_spectrum';
test_out_dir_mfcc = 'C:\Users\USER\Desktop\test\fina_mfcc';

test_pathes = listfile(test_input_dir, '.wav');
test_path = test_pathes{1};
test_dic = [];

if exist(test_out_dir_time_series, 'dir')
    rmdir(test_out_dir_time_series,'s');
end
if exist(test_out_dir_spectrum, 'dir')
    rmdir(test_out_dir_spectrum,'s');
end
if exist(test_out_dir_mel_spectrum, 'dir')
    rmdir(test_out_dir_mel_spectrum,'s');
end
if exist(test_out_dir_mfcc, 'dir')
    rmdir(test_out_dir_mfcc,'s');
end

%% test aw extraction #1 for time series
processes = {
    'Input;Representation=Time Series;Window Size=1024;Hop Size=512;';
    };

final_time_series = audio_word_encode(test_path, processes, test_dic, test_out_dir_time_series);

%% test aw extraction #1 for spectrum
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    };

final_spectrum = audio_word_encode(test_path, processes, test_dic, test_out_dir_spectrum);

%% test aw extraction #1 for mel-spectrum
processes = {
    'Input;Representation=Mel-spectrum;Window Size=1024;Hop Size=512;Number of Filter=80;';
    };

final_mel_spectrum = audio_word_encode(test_path, processes, test_dic, test_out_dir_mel_spectrum);

%% test aw extraction #1 for mfcc
processes = {
    'Input;Representation=MFCC;Window Size=1024;Hop Size=512;Number of Filter=80;Number of Coefficient=40;';
    };

final_mfcc = audio_word_encode(test_path, processes, test_dic, test_out_dir_mfcc);