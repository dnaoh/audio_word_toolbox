clear
clc

addpath('audio_word_encode');
addpath('dictionary_train');

test_input_dir = 'C:\Users\USER\Desktop\test\_wav';
test_out_dir_kmeans = 'C:\Users\USER\Desktop\test\fina_kmeans';
test_temp_dir = 'C:\Users\USER\Desktop\test\temp_kmeans_odl';
test_out_dir_odl = 'C:\Users\USER\Desktop\test\fina_odl';

test_pathes = listfile(test_input_dir, '.wav');
test_dic_kmeans = 'C:\Users\USER\Desktop\test\test_dic_kmeans.mat';
test_dic_odl = 'C:\Users\USER\Desktop\test\test_dic_odl.mat';

if exist(test_dic_kmeans, 'file')
    delete(test_dic_kmeans);
end
if exist(test_dic_odl, 'file')
    delete(test_dic_odl);
end
if exist(test_out_dir_kmeans, 'dir')
    rmdir(test_out_dir_kmeans,'s');
end
if exist(test_temp_dir, 'dir')
    rmdir(test_temp_dir,'s');
end
if exist(test_out_dir_odl, 'dir')
    rmdir(test_out_dir_odl,'s');
end

% test aw extraction #1 for kmeans
test_pathes_out = cell(size(test_pathes));
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    };
for i = 1:length(test_pathes)
    test_path = test_pathes{i};
    test_pathes_out{i} = audio_word_encode(test_path, processes, test_dic_kmeans, test_temp_dir);
end

%% kmeans
% test dictionary training #1 for kmeans
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Sparse Coding;Dictionary=k-means;Dictionary Size=1024;';
    };
test_dic_kmeans = dictionary_train(test_pathes_out, processes, test_dic_kmeans);

% final aw extraction for kmeans
final_aw_kmeans = audio_word_encode(test_pathes{1}, processes, test_dic_kmeans, test_out_dir_kmeans);

%% ODL
% test dictionary training #1 for odl
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Sparse Coding;Dictionary=ODL;Dictionary Size=1024;';
    };
test_dic_odl = dictionary_train(test_pathes_out, processes, test_dic_odl);

% final aw extraction for odl
final_aw_odl = audio_word_encode(test_pathes{1}, processes, test_dic_odl, test_out_dir_odl);