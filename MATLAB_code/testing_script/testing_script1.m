clear
clc

addpath('audio_word_encode');
addpath('dictionary_train');

test_input_dir = 'C:\Users\USER\Desktop\test\_wav';
test_out_dir = 'C:\Users\USER\Desktop\test\final';
test_temp1_dir = 'C:\Users\USER\Desktop\test\temp1';
test_temp2_dir = 'C:\Users\USER\Desktop\test\temp2';

test_pathes = listfile(test_input_dir, '.wav');
test_dic = 'C:\Users\USER\Desktop\test\test_dic.mat';
test_pathes_out = cell(size(test_pathes));

if exist(test_dic, 'file')
    delete(test_dic);
end
if exist(test_out_dir, 'dir')
    rmdir(test_out_dir,'s');
end
if exist(test_temp1_dir, 'dir')
    rmdir(test_temp1_dir,'s');
end
if exist(test_temp2_dir, 'dir')
    rmdir(test_temp2_dir,'s');
end

%% test aw extraction #1
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    };
for i = 1:length(test_pathes)
    test_path = test_pathes{i};
    test_pathes_out{i} = audio_word_encode(test_path, processes, test_dic, test_temp1_dir);
end

%% test dictionary training #1
% test_pathes_out = listfile(test_temp1_dir, '.mat');
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=1024;';
    };
test_dic = dictionary_train(test_pathes_out, processes, test_dic);

%% test aw extraction #2
% test_pathes_out = listfile(test_temp1_dir, '.mat');
processes = {
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=1024;';
    'Rectification;Process=Absolute Value;';
    'Pooling;Process=Plain;Function=Mean;Level=Segment;Window Size=64;Hop Size=32;';
    'Other;Process=Normalization;Method=nth Root;Degree of Root=3;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    };
for i = 1:length(test_pathes_out)
    test_path_out = test_pathes_out{i};
    test_pathes_out{i} = audio_word_encode(test_path_out, processes, test_dic, test_temp2_dir);
end

%% test dictionary training #2
% test_pathes_out = listfile(test_temp2_dir, '.mat');
processes = {
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=1024;';
    'Rectification;Process=Absolute Value;';
    'Pooling;Process=Plain;Function=Mean;Level=Segment;Window Size=64;Hop Size=32;';
    'Other;Process=Normalization;Method=nth Root;Degree of Root=3;';
    'Other;Process=Normalization;Method=Unit 2-norm;';
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=2048;';
    };
test_dic = dictionary_train(test_pathes_out, processes, test_dic);

%% final aw extraction
processes = {
    'Input;Representation=Spectrum;Window Size=1024;Hop Size=512;'
    'Other;Process=Normalization;Method=Unit 2-norm;'
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=1024;'
    'Rectification;Process=Absolute Value;'
    'Pooling;Process=Plain;Function=Mean;Level=Segment;Window Size=64;Hop Size=32;'
    'Other;Process=Normalization;Method=nth Root;Degree of Root=3;'
    'Other;Process=Normalization;Method=Unit 2-norm;'
    'Encoding;Process=Sparse Coding;Dictionary=Random Samples;Dictionary Size=2048;'
    'Rectification;Process=Polar Split;'
    'Pooling;Process=Plain;Function=Mean;Level=Clip;'
    'Other;Process=Normalization;Method=nth Root;Degree of Root=3;'
    };

final_aw = audio_word_encode(test_pathes{1}, processes, test_dic, test_out_dir);