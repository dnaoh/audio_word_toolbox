function dictionary = random_sample(data_pathes, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Dictionary Size')
        dictionary_size = str2double(process_option{i + 1});
    end
end
verbose = true;
number_of_data = length(data_pathes);

% counting number of samples
length_of_data = zeros(number_of_data, 1);
for i = 1:number_of_data
    data_path = data_pathes{i};
    if verbose
        fprintf('Counting %s ...', data_path);
    end
    length_of_data(i) = count_examples(data_path);
end
total_length_of_data = sum(length_of_data);

% check if the number of sample is too small
if total_length_of_data < dictionary_size
    dictionary = -1; % maybe should be changed to error message
    return;
end

% under sampling
random_vector = randperm(total_length_of_data);
random_vector_mask = false(total_length_of_data, 1);
random_vector_mask(random_vector(1:dictionary_size)) = true;
number_of_assignment = zeros(number_of_data, 1);
index_st = 1;
for i = 1:number_of_data
    number_of_assignment(i) = ...
        sum(random_vector_mask(index_st:index_st+length_of_data(i)-1));
    index_st = index_st+length_of_data(i);
end

% extract examples
dictionary = cell(1, number_of_data);
for i = 1:number_of_data
    data_path = data_pathes{i};
    if verbose
        fprintf('Loading %s ...', data_path);
    end
    if number_of_assignment(i)~= 0
        dictionary{i} = read_random_examples(data_path, number_of_assignment(i));
    end
end
dictionary = cell2mat(dictionary);
dictionary = dictionary(:, randperm(size(dictionary, 2)));

function number_of_data = count_examples(data_path)
% load features
try
    data = load(data_path);
    data = data.data;
catch
    number_of_data = 0;
    return;
end

% check if the data is valid
if size(data, 1) == 1
    number_of_data = 0;
    return;
end

% remove all zero frames
data = data(:, sum(abs(data), 1) > 0);

% return number of feature
number_of_data = size(data, 2);

function data = read_random_examples(data_path, number_of_assignment)
% load features
data = loadadata(data_path);

% remove all zero frames
data = data(:, sum(abs(data), 1) > 0);

% random sample
number_of_data = size(data, 2);
assignment = randperm(number_of_data);
data = data(:, assignment(1:number_of_assignment));

function data = loadadata(data_path)
data = load(data_path);
data = data.data;