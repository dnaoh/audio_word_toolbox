function dictionary_path = dictionary_train(in_pathes, processes, dictionary_path)
% check if dictionary path exist
dictionary = [];
dictionary_processes = [];
if exist(dictionary_path, 'file')
    load(dictionary_path);
end

% get current training process
target_process = processes{end};

% parse the process string
process_splited = strsplit(target_process(1:end-1), ';');
process_option = cell((length(process_splited) - 1)*2, 1);
for i = 2:length(process_splited)
    process_option_splited = strsplit(process_splited{i}, '=');
    process_option{(i-2)*2 + 1} = process_option_splited{1};
    process_option{(i-1) * 2} = process_option_splited{2};
end

% parse out dictinoary type
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Dictionary')
        target_method = process_option{i + 1};
    end
end

% train the dictionary
if strcmpi(target_method, 'Random Samples')
    dictionary_ = random_sample(in_pathes, process_option);
elseif strcmpi(target_method, 'k-means')
    dictionary_ = k_means(in_pathes, process_option);
elseif strcmpi(target_method, 'ODL')
    dictionary_ = online_dictionary_learning(in_pathes, process_option);
end

% save the trained dictionary
if dictionary_ == -1
    dictionary_path = 'ERROR: Your number of example is smaller than the desired dictionary size';
    fprintf(dictionary_path);
    return;
end
if issparse(dictionary_)
    dictionary_ = full(dictionary_);
end
if ~iscell(dictionary_)
    dictionary_ = {dictionary_};
end
dictionary = vertcat(dictionary, dictionary_);
if iscell(dictionary_processes)
    dictionary_processes = {
        vertcat(dictionary_processes{:});
        vertcat(dictionary_processes{end}, processes(2:end));
        };
else
    dictionary_processes = {processes};
end
[~] = dictionary;
[~] = dictionary_processes;
save(dictionary_path, 'dictionary', 'dictionary_processes');