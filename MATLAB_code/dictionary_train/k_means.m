function dictionary = k_means(data_pathes, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Dictionary Size')
        dictionary_size = str2double(process_option{i + 1});
    end
end

% initialization
number_of_batch = 3;
number_of_data_pathes = length(data_pathes);
data_dimension = size(loadadata(data_pathes{1}), 1);
memory_info = memory();
memory_limit = floor(memory_info.MaxPossibleArrayBytes*0.1 / (data_dimension*8));
batch_size = min(512*200, memory_limit);
verbose = true;

% loop accross different batches
for i = 1:number_of_batch
    if verbose
        fprintf('Training mini-batch %d', i);
    end
    
    % undersampling
    assignment = ceil(rand(batch_size, 1) * number_of_data_pathes);
    training_data = cell(1, number_of_data_pathes);
    for j = 1:number_of_data_pathes
        data_path = data_pathes{j};
        if verbose
            fprintf('\tProcessing %s ...', data_path);
        end
        number_of_assignment = sum(assignment == j);
        if number_of_assignment ~= 0
            training_data{j} = read_random_examples(...
                data_path, number_of_assignment);
        end
    end
    training_data = cell2mat(training_data);
    random_vector = randperm(size(training_data, 2));
    training_data = training_data(:, random_vector);
    
    % check if the number of sample is too small
    if size(training_data, 2) < dictionary_size
        dictionary = -1; % maybe should be changed to error message
        return;
    end
    
    % mini-batch k-means training
    fprintf('\tTraining k-means dictionary with mini-batch %d ...', i);
    if ~exist('dictionary_property', 'var')
        [dictionary, dictionary_property] = ...
            mini_batch_kmeans(training_data, dictionary_size, 'Verbose', verbose);
    else
        [dictionary, dictionary_property] = ...
            mini_batch_kmeans(training_data, dictionary_size, 'Verbose', verbose, ...
            'Dictioanry', dictionary, 'Dictioanry_Property', dictionary_property);
    end
end

function data = read_random_examples(data_path, number_of_assignment)
% load features
try
    data = loadadata(data_path);
catch
    data = [];
    return;
end

% check if the data is valid
if size(data, 1) == 1
    data = [];
    return;
end

% random sample
data_length = size(data, 2);
random_vector = randperm(data_length);
if number_of_assignment > data_length
   number_of_assignment = data_length; 
end
data = data(:, random_vector(1:number_of_assignment));

function data = loadadata(data_path)
data = load(data_path);
data = data.data;