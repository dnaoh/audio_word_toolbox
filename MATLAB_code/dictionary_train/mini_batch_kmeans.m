function [dictionary, dictionary_property] = ...
    mini_batch_kmeans(X, K, varargin)
% initialize
dictionary = [];
iteration = 200;
batch_size = 512;
verbose = true;
for i = 1:length(varargin)
    if strcmp(varargin{i}, 'Dictioanry')
        dictionary = varargin{i + 1};
    end
    if strcmp(varargin{i}, 'Dictioanry_Property')
        dictionary_property = varargin{i + 1};
    end
    if strcmp(varargin{i}, 'Iteration')
        iteration = varargin{i + 1};
    end
    if strcmp(varargin{i}, 'Batch_Size')
        batch_size = varargin{i + 1};
    end
    if strcmp(varargin{i}, 'Verbose')
        verbose = varargin{i + 1};
    end
end

% initialized a dictionary if it is not given
if isempty(dictionary)
    dictionary = kmeans_seeding(X, K);
    dictionary_property = zeros(1, K);
end

% iteratively update the dictionary
for i = 1:iteration
    if verbose
        fprintf('Iteration %d ... ', i);
    end
    
    % get mini batch
    randem_vector = randperm(size(X, 2));
    mini_batch = X(:, randem_vector(1:min(batch_size, size(X, 2))));
    
    % identify the nearest dictionary item
    index = zeros(1, K);
    for j = 1:size(mini_batch, 2)
        index(j) = get_nearest_element_index(mini_batch(:, j), dictionary);
    end
    
    % update the dictionary
    for j = 1:size(mini_batch, 2)
        dictionary_property(index(j)) = ...
            dictionary_property(index(j)) + 1;
        learning_rate = 1/dictionary_property(index(j));
        dictionary(:, index(j)) = (1-learning_rate)*dictionary(:, index(j)) ...
            + learning_rate*mini_batch(:, j);
    end
    
    if verbose
        fprintf('done!');
    end
end

function dictionary = kmeans_seeding(X, K)
randem_vector = randperm(size(X, 2));
dictionary = X(:, randem_vector(1:K));

function index = get_nearest_element_index(x, dictionary)
distance = zeros(1, size(dictionary, 2));
for i = 1:size(dictionary, 2)
    distance(i) = calculate_distance(x, dictionary(:, i));
end
[~, index] = min(distance);

function distance = calculate_distance(x, y)
difference = x - y;
distance = sqrt(difference' * difference);