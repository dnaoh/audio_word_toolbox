clear
clc

addpath(genpath('toolbox'));

m_files = {
    'audio_word_encode';
    'dictionary_train';
    };
dll_files = {
    'AudioWordEncode';
    'DictionaryTrain';
    };

for i = 1:length(m_files)
    m_file = m_files{i};
    dll_file = dll_files{i};
    m_file_path = fullfile('.', m_file, [m_file, '.m']);
    dll_file_path = fullfile('.', [m_file, '_compiled'], [dll_file, '.dll']);
    dll_file_dir = fullfile('.', [m_file, '_compiled']);
    
    fprintf('Check dir %s ... ', dll_file_dir);
    if exist(dll_file_dir, 'dir')
        rmdir(dll_file_dir, 's');
    end
    if ~exist(dll_file_dir, 'dir')
        mkdir(dll_file_dir);
    end
    fprintf('done!\n');
    
    fprintf('Compiling %s ... ', dll_file_path);
    cmd = sprintf('mcc -N -d %s -W ''dotnet:%s,%sClass,4.0,private'' -T link:lib %s', ...
        dll_file_dir, dll_file, dll_file, m_file_path);
    eval(cmd);
    fprintf('done!\n');
end