function [W] = Lasso_screening(D, X, lambda)
% Author: Ping-Keng Jao
% Release date: 2013/09/04

% This function is an implementatuib of 
%'Learning sparse Representations of High Dimentsional Data on Large Scale'

% Parameters:
%    D is the dictionary,  an m by k matrix. Each column must be normalized
%    to 1 with Euclidean norm.
%    X is the data matrix, an m by n matrix. Each column must be normalized
%    to 1 with Euclidean norm.
%    norFrame tells whether to normalize frame in this function

[m, k]= size(D);
n = size(X, 2);
W = zeros(k, n);

if nargin < 3, lambda = 1./sqrt(m); end
%% Phase1: Find lambda_max and b_max, the corresponding basis (atom) acheive lambda_max with the correct sign
C = D'*X; % correlation, the elements in the same column of C correspond to the same data vector
          % C is k by n
absC = abs(C);               
% find b_max
b_max = zeros(m, n);
for i = 1 : n
    lambda_max(1, i) = max(absC(:,i));
    idx = find(absC(:,i) == lambda_max(1,i));
    b_max(:, i) = D(:, idx(1));
    if b_max(:,i)'*X(:,i) < 0
        b_max(:,i) = -b_max(:,i);
    end
end

% lambda2 is used for the case that lambda > lambda_max, in that case, we
% will set it equal to lambda_max
lambda2 = min(lambda, lambda_max);
%% Phase 2: Determine threshold and casts the screening

thres = zeros(n, 1); %threshold
thres = lambda2.*(1-sqrt(1./(lambda_max).^2-1).*(lambda_max./lambda2-1));
thres(lambda_max == lambda2) = lambda2(lambda_max == lambda2);
thres = real(thres);
T = abs( C - repmat((lambda_max - lambda2), k, 1) .*  (D'*b_max) );
thres = repmat(thres, [size(W,1) 1]);
W = real(T-thres) >= 0;        



end