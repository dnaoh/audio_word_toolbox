function [coef_screen] = encode_with_screening(W, codebook, feature, param)

coef_screen = zeros(size(codebook, 2), size(feature, 2));

W = sum(W, 2);
W = W > 0;
table = cumsum(W);
all_reject = false;
temp_coef_screen = sparse(zeros(size(W, 1), size(feature, 2) ));
coef_screen = sparse(zeros(size(codebook, 2), size(feature, 2) ));
if sum(W(:,1)) == 0    
    W = true(size(W));
    all_reject = true;
end
new_codebook = codebook(:, W);
temp_coef_screen = mexLasso(feature, new_codebook, param);
if all_reject
    coef_screen = temp_coef_screen;
else    
    target_dim = 1;
    for idx = 1 : size(W, 1)
        if target_dim == table(idx, 1)
            trans_table(target_dim) = idx;
            target_dim = target_dim + 1;
        end
    end
    coef_screen(trans_table, :) = temp_coef_screen;
end

reject_ratio = sum(W)/size(codebook, 2);
reject_ratio = 1 - reject_ratio;


end