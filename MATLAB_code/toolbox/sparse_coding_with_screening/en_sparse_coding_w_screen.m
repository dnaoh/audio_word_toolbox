function [encoding] = en_sparse_coding_w_screen(data, dictionary, process_option)
%% initial parameter
%paramter of SPAMS
parameter.pos = 0;
parameter.lambda = 1 / sqrt(size(data, 1));

screen_lambda = parameter.lambda;
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Parameter of Screening')
        screen_lambda = str2double(process_option{i + 1});
    end    
end

W = Lasso_screening(dictionary, data, screen_lambda);
encoding = encode_with_screening(W, dictionary, data, parameter);
