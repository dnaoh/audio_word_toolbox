function data = ot_normalization(data, process_option)
% parse option
degree_of_root = [];
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Method')
        method = process_option{i + 1};
    end
    if strcmpi(process_option{i}, 'Degree of Root')
        degree_of_root = str2double(process_option{i + 1});
    end
end

% process the input data
data = data';
if strcmpi(method, 'Sum to One')
    data = nan_inf_to_zero(bsxfun(@rdivide, data, sum(data, 2)));
elseif strcmpi(method, 'Unit 2-norm')
    data = nan_inf_to_zero(bsxfun(@rdivide, data, sqrt(sum(data.^2, 2))));
elseif strcmpi(method, 'nth Root')
    if rem(degree_of_root, 2)
        data = abs(data);
    end
    data = nan_inf_to_zero(nthroot(data, degree_of_root));
end
data = data';

function x = nan_inf_to_zero(x)
x(isinf(x)) = 0;
x(isnan(x)) = 0;