function data = in_input_layer(in_path, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Representation')
        representation = process_option{i + 1};
    end
end

% process the input data accordingly
if strcmpi(representation, 'Time Series')
    data = in_time_series(in_path, process_option);
elseif strcmpi(representation, 'Spectrum')
    data = in_spectrum(in_path, process_option);
elseif strcmpi(representation, 'Cepstrum')
    data = in_cepstrum(in_path, process_option);
elseif strcmpi(representation, 'Mel-spectrum')
    data = in_mel_spectrum(in_path, process_option);
elseif strcmpi(representation, 'MFCC')
    data = in_mfcc(in_path, process_option);
end