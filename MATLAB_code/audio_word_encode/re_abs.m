function data = re_abs(data, process_option)
[~] = process_option; % process_option is not used in this function
data = abs(data);