function data_ = po_plain(data, process_option)
% parse option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Function')
        function_ = process_option{i + 1};
    end
    if strcmpi(process_option{i}, 'Level')
        level = process_option{i + 1};
    end
    if strcmpi(process_option{i}, 'Window Size')
        window_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Hop Size')
        hop_size = str2double(process_option{i + 1});
    end
end

% set pooling function
if strcmpi(function_, 'Sum')
    function_ = @(x) sum(x, 2);
elseif strcmpi(function_, 'Mean')
    function_ = @(x) mean(x, 2);
elseif strcmpi(function_, 'Max')
    function_ = @(x) max(x, [], 2);
end

% pool the input data
if strcmpi(level, 'Segment')
    data_ = segment_level_pooling(data, window_size, hop_size, function_);
elseif strcmpi(level, 'Clip')
    data_ = function_(data);
end

%% function for segment-level pooling
function data_ = segment_level_pooling(data, window_size, hop_size, function_)
% if feat is shorter than win, pool it directly
feat_n = size(data, 2);
if feat_n < window_size
    data_ = function_(data);
    return;
end

% calculate the start/end point for each window
st = 1:hop_size:feat_n;
ed = st + window_size - 1;
while ed(end) > feat_n
    st = st(1:end - 1);
    ed = ed(1:end - 1);
end
feat_n = length(st);

% apply fun to each window
data_ = cell(1, feat_n);
for i = 1:feat_n
    data_{i} = function_(data(:, st(i):ed(i)));
end
data_ = cell2mat(data_);