function data = ot_conscutive_frame(data, process_option)
% parse option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Window Size')
        window_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Hop Size')
        hop_size = str2double(process_option{i + 1});
    end
end

% calculate the start/end point for each window
data_length = size(data, 2);
index_start = 1:hop_size:data_length;
index_end = index_start + window_size - 1;
while index_end(end) > data_length
    index_start = index_start(1:end - 1);
    index_end = index_end(1:end - 1);
end

% concatenate the input data
data_ = cell(window_size, 1);
for i = 1:window_size
    data_{i} = data(:, index_start + i - 1);
end
data = cell2mat(data_);