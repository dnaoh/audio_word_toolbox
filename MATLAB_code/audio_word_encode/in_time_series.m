function data = in_time_series(in_path, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Window Size')
        window_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Hop Size')
        hop_size = str2double(process_option{i + 1});
    end
end

% read wave
try
    [wav, samp_rate] = audioread(in_path);
catch
    data = 0;
    return;
end

% merge left/right channel
wav = mean(wav, 2);
if ~any(wav)
    data = 0;
    return;
end

% wave validation from JY
wav = wav_validation(wav, samp_rate);

% nromalize wave with RMS
wav = wav ./ sqrt(sum(wav .^ 2) / length(wav));
wav(isnan(wav)) = 0;

% calculate the start/end point for each frame
wav_length = length(wav);
if wav_length < window_size
    data = 0;
    return;
end
st = 1:hop_size:wav_length;
ed = st + window_size - 1;
while ed(end) > wav_length
    st = st(1:end - 1);
    ed = ed(1:end - 1);
end
number_of_window = length(st);

% extract the spectrum for each frame
data = cell(number_of_window, 1);
for i = 1:number_of_window
    data{i} = wav(st(i):ed(i))';
end
data = cell2mat(data)';

function y = wav_validation(y, Fs)
minLen=8;
framesize =128;
A=10;
nbits = 32;

llen=length(y);
if llen< minLen*Fs
    % concatenate zeros to the end of y if the length of y is not long enough
    lackLen= 10*Fs - llen;
    y(llen+1: llen+lackLen) = zeros(lackLen, 1);
end
num = floor(length(y)/framesize);
for j =1:num
    st=(j-1)*framesize+1;
    tmp = y(st:st+framesize-1);
    if sum(tmp)==0
        tmp = round(rand(framesize,1)*A) - (A/2);
        y(st:st+framesize-1) = tmp/power(2,nbits-1);
    end
end