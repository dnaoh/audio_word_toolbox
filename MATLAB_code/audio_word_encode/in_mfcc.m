function data = in_mfcc(in_path, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Window Size')
        window_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Hop Size')
        hop_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Number of Filter')
        number_of_filter = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Number of Coefficient')
        number_of_coefficient = str2double(process_option{i + 1});
    end
end

% read wave
try
    [wav, samp_rate] = audioread(in_path);
catch
    data = 0;
    return;
end

% merge left/right channel
wav = mean(wav, 2);
if ~any(wav)
    data = 0;
    return;
end

% wave validation from JY
wav = wav_validation(wav, samp_rate);

% nromalize wave with RMS
wav = wav ./ sqrt(sum(wav .^ 2) / length(wav));
wav(isnan(wav)) = 0;

% calculate the start/end point for each frame
wav_length = length(wav);
if wav_length < window_size
    data = 0;
    return;
end
st = 1:hop_size:wav_length;
ed = st + window_size - 1;
while ed(end) > wav_length
    st = st(1:end - 1);
    ed = ed(1:end - 1);
end
number_of_window = length(st);

% extract the spectrum for each frame
data = cell(number_of_window, 1);
widnow_function = 0.54 - 0.46*cos(2*pi*(0:window_size-1)/(window_size-1));
for i = 1:number_of_window
    data{i} = wav(st(i):ed(i))';
    data{i} = abs(fft(data{i}.*widnow_function));
    data{i} = data{i}(:, round(window_size / 2) + 1:end);
end
data = cell2mat(data)';

% triangular filter bank
filter_bank = triangular_filter_bank(...
    samp_rate, window_size, number_of_filter);
data = filter_bank * data;

% convert to db\cbrt scale
% spec = 20 * log10(spec + 1e-16);
data = nthroot(data, 3);
data(isnan(data)) = 0;
data(isinf(data)) = 0;

% discrete cosine transform
dct_matrix = 1/sqrt(number_of_filter/2)*cos((0:(number_of_coefficient-1))' * ...
    (2*(0:(number_of_filter-1))+1) * pi/2/number_of_filter);
dct_matrix(1,:) = dct_matrix(1,:) * sqrt(2)/2;
data = dct_matrix * data;
data = flipud(data);
data(isnan(data)) = 0;
data(isinf(data)) = 0;

% replace zero norm frames with eps
mfcc_2norm = sqrt(sum(data .^ 2, 1));
data(:, mfcc_2norm == 0) = 1e-16;

function y = wav_validation(y, Fs)
minLen=8;
framesize =128;
A=10;
nbits = 32;

llen=length(y);
if llen< minLen*Fs
    % concatenate zeros to the end of y if the length of y is not long enough
    lackLen= 10*Fs - llen;
    y(llen+1: llen+lackLen) = zeros(lackLen, 1);
end
num = floor(length(y)/framesize);
for j =1:num
    st=(j-1)*framesize+1;
    tmp = y(st:st+framesize-1);
    if sum(tmp)==0
        tmp = round(rand(framesize,1)*A) - (A/2);
        y(st:st+framesize-1) = tmp/power(2,nbits-1);
    end
end

function filter_bank = triangular_filter_bank(samp_rate, win, mfcc_n)
hz2mel = @(hz) (1127*log(1+hz/700)); % Hertz to mel
fftn = round(win / 2);
hz_scale = linspace(0, round(samp_rate/2), fftn);
hz_scale = hz_scale(2:end);
mel_scale = hz2mel(hz_scale);
mel_scale_cutoff = linspace(mel_scale(1), mel_scale(end), mfcc_n+2);
filter_bank = zeros(mfcc_n, fftn);
for i = 1:mfcc_n
    k = mel_scale >= mel_scale_cutoff(i) & mel_scale <= mel_scale_cutoff(i+1);
    filter_bank(i, k) = (mel_scale(k)-mel_scale_cutoff(i)) / ...
        (mel_scale_cutoff(i+1)-mel_scale_cutoff(i));
    k = mel_scale >= mel_scale_cutoff(i+1) & mel_scale <= mel_scale_cutoff(i+2);
    filter_bank(i, k) = (mel_scale_cutoff(i+2)-mel_scale(k)) / ...
        (mel_scale_cutoff(i+2)-mel_scale_cutoff(i+1));
    % filter_bank(i, :) = filter_bank(i, :) ./ sum(filter_bank(i, :));
end