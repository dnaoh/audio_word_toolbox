function data_ = en_encoding_layer(data, dictionary, process_option)
% parse process type
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Process')
        process = process_option{i + 1};
    end
end

% process the input data accordingly
if strcmpi(process, 'Vector Quantization (VQ)')
    data_ = en_vector_quantization(data, dictionary, process_option);
elseif strcmpi(process, 'Triangle Coding (TC)')
    data_ = en_triangle_coding(data, dictionary, process_option);
elseif strcmpi(process, 'Sparse Coding (SC)')
    data_ = en_sparse_coding(data, dictionary, process_option);
elseif strcmpi(process, 'SC w/ Screening (SCS)')	
	data_ = en_sparse_coding_w_screen(data, dictionary, process_option);
end