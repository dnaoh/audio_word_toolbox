function data_ = en_sparse_coding(data, dictionary, process_option)
[~] = process_option; % process_option is not used in this function
parameter.pos = 0;
parameter.lambda = 1 / sqrt(size(data, 1));
if issparse(data)
    data = full(data);
end
data_ = mexLasso(data, dictionary, parameter);