% out_path = audio_word_encode(in_path, processes, dictionary_path, out_dir)
% Input
%   in_path
%   processes
%   dictionary_path
%   out_dir
% Output
%   out_dir
%       out_path == '0' => the input audio clip/representation is skipped
%       out_path == '-1' => error, the whole audio word extraction process should be stop 
%

function out_path = audio_word_encode(in_path, processes, dictionary_path, out_dir, out_extension)
% check if the output dir exists, create if not
if ~exist(out_dir, 'dir')
    mkdir(out_dir);
end

% construct the output path
[~, file_name, file_extension] = fileparts(in_path);
if strcmpi(out_extension, '*.mat')
    out_path = fullfile(out_dir, [file_name, '.mat']);
elseif strcmpi(out_extension, '*.csv')
    out_path = fullfile(out_dir, [file_name, '.csv']);
end

% check if the input file exist
if ~exist(in_path, 'file')
    out_path = 'WARRNING: the input file does not exist';
    fprintf(out_path);
    return;
end

% read the file
data_process = [];
if strcmpi(file_extension, '.mat')
    try
        data = load(in_path);
    catch
        out_path = 'WARRNING: fail to read the input file';
        fprintf(out_path);
        return;
    end
    data_process = data.data_process;
    data = data.data;
end

% process the input files
dictionary_index = 1; % in case that there are multiple dictionary
fprintf('Extracting: %s ... ', in_path);
number_of_preprocess = length(data_process);
data_process = vertcat(data_process, cell(length(processes), 1));
for i = 1:length(processes)
    process = processes{i};
    data_process{number_of_preprocess + i} = process;
    fprintf('\t%s ', process);
    
    % parse the process string
    process_splited = strsplit(process(1:end-1), ';');
    process_type = process_splited{1};
    process_option = cell((length(process_splited) - 1)*2, 1);
    for j = 2:length(process_splited)
        process_option_splited = strsplit(process_splited{j}, '=');
        process_option{(j-2)*2 + 1} = process_option_splited{1};
        process_option{(j-1) * 2} = process_option_splited{2};
    end
    
    % process the data accordingly
    switch process_type
        case 'Input'
            data = in_input_layer(in_path, process_option);
            
        case 'Encoding'
            % load dictionary
            dictionary = [];
            dictionary_processes = [];
            load(dictionary_path);
            
            % check if the dictionary exist
            if length(dictionary) < dictionary_index
                out_path = 'ERROR: the dictionary is not usable';
                fprintf('\t%s ', out_path);
                return;
            end
            
            % check if the loaded dictionary's setting is the same
            if ~all(strcmpi(dictionary_processes{dictionary_index}, ...
                    data_process(1:length(dictionary_processes{dictionary_index}))))
                out_path = 'WARRNING: the process setting of the dictionary is different than the current process setting';
                fprintf('\t%s ', out_path);
            end
            
            % check if the loaded dictionary is usable
            data_dimension = size(data, 1);
            dictionary_dimension = size(dictionary{dictionary_index}, 1);
            if data_dimension ~= dictionary_dimension
                out_path = 'ERROR: the dictionary is not usable';
                fprintf('\t%s ', out_path);
                return;
            end
            
            % encode
            data = en_encoding_layer(data, dictionary{dictionary_index}, ...
                process_option);
            dictionary_index = dictionary_index + 1;
            
        case 'Pooling'
            data = po_pooling_layer(data, process_option);
            
        case 'Rectification'
            data = re_rectification_layer(data, process_option);
            
        case 'Other'
            data = ot_other_layer(data, process_option);
            
    end
    
    if data == 0
        out_path = 'WARRNING: the result data is empty';
        fprintf('\t%s ', out_path);
        return;
    end
end

if strcmpi(out_extension, '*.mat')
    save(out_path, 'data', 'data_process');
else
    if issparse(data)
        data = full(data);
    end
    csvwrite(out_path, data);
end