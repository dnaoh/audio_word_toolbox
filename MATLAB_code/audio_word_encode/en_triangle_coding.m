function data_ = en_triangle_coding(data, dictionary, process_option)
[~] = process_option; % process_option is not used in this function
data_ = zeros(size(dictionary, 2), size(data, 2));
for i = 1:size(data, 2)
    % distance computation
    distance = zeros(1, size(dictionary, 2));
    for j = 1:size(dictionary, 2)
        difference = data(:, i) - dictionary(:, j);
        distance(j) = sqrt(difference' * difference);
    end
    
    % select element in dictionary
    distance_mean = mean(distance);
    data_(:, i) = ones(size(distance))*distance_mean - distance;
    data_(data_(:, i) < 0, i) = 0;
end
data_ = sparse(data_);