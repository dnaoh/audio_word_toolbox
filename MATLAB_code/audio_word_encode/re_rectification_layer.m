function data = re_rectification_layer(data, process_option)
% parse process option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Process')
        process = process_option{i + 1};
    end
end

% process the input data
if strcmpi(process, 'Absolute Value')
    data = re_abs(data, process_option);
elseif strcmpi(process, 'Polar Split')
    data = re_polar_split(data, process_option);
end