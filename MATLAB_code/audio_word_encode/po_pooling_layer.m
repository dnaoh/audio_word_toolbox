function data_ = po_pooling_layer(data, process_option)
% parse process type
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Process')
        process = process_option{i + 1};
    end
end

% process the input data accordingly
if strcmpi(process, 'Pyramid')
    data_ = po_pyramid(data, process_option);
elseif strcmpi(process, 'Plain')
    data_ = po_plain(data, process_option);
end