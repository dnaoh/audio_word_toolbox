function data_ = po_pyramid(data, process_option)
% parse option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Number of Scale')
        number_of_scale = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Function')
        function_ = process_option{i + 1};
    end
    if strcmpi(process_option{i}, 'Level')
        level = process_option{i + 1};
    end
    if strcmpi(process_option{i}, 'Window Size')
        window_size = str2double(process_option{i + 1});
    end
    if strcmpi(process_option{i}, 'Hop Size')
        hop_size = str2double(process_option{i + 1});
    end
end

% set pooling function
if strcmpi(function_, 'Sum')
    function_ = @(x) sum(x, 2);
elseif strcmpi(function_, 'Mean')
    function_ = @(x) mean(x, 2);
elseif strcmpi(function_, 'Max')
    function_ = @(x) max(x, [], 2);
end
function_ = @(x) temporal_pyramid_pooling(x, number_of_scale, function_);

% pool the input data
if strcmpi(level, 'Segment')
    data_ = segment_level_pooling(data, window_size, hop_size, function_);
elseif strcmpi(level, 'Clip')
    data_ = function_(data);
end

%% functions for temporal pyramid pooling
function data_ = temporal_pyramid_pooling(data, number_of_scale, function_)
% split the input feature matrix along the temporal direction
data = pyramid_spliting(data, number_of_scale);

% calculate the weight for each level
weight = pyramid_weight(number_of_scale);

% pool and apply the weight
for j = 1:length(data)
    data{j} = function_(data{j});
    data{j} = data{j} * weight(j);
end

% combine the pooled feature
data_ = cell2mat(data);

function data_ = pyramid_spliting(data, number_of_scale)
% split the input feature matrix in half along the temporal direction
if number_of_scale == 1
    data_ = data;
    return;
else
    mid_pt = round(size(data, 2) / 2);
    part_1 = data(:, 1:mid_pt);
    part_2 = data(:, mid_pt + 1:end);
    number_of_scale_ = number_of_scale - 1;
    data_1 = pyramid_spliting(part_1, number_of_scale_);
    data_2 = pyramid_spliting(part_2, number_of_scale_);
end

% concatenate with finer level vector
if iscell(data_1)
    data_ = cell(1 + length(data_1)*2, 1);
    data_{1} = data;
    part1_index = [2, sort(index_propagation(2, number_of_scale_-1))];
    part2_index = [3, sort(index_propagation(3, number_of_scale_-1))];
    data_(part1_index) = data_1;
    data_(part2_index) = data_2;
else
    data_ = {
        data;
        data_1;
        data_2;
        };
end

function index = index_propagation(index, number_of_scale)
% propergate the index number for partition
index = [index * 2, index * 2 + 1];
if number_of_scale > 1
    index = [index, index_propagation(index(1), number_of_scale-1), ...
        index_propagation(index(2), number_of_scale-1)];
end

function weight = pyramid_weight(number_of_scale)
scale_level_number = zeros(sum(2.^(0:number_of_scale-1)), 1);
st_idx = 1;

% calculate level number
for i = 0:number_of_scale-1
    scale_level_number(st_idx:st_idx + 2^i - 1) = i;
    st_idx = st_idx + 2^i;
end

% calculate weight
weight = [
    1/(2^max(scale_level_number)); 
    1 ./ 2.^(number_of_scale - scale_level_number(2:end));
    ];

%% function for segment-level pooling
function data_ = segment_level_pooling(data, window_size, hop_size, function_)
% if feat is shorter than win, pool it directly
feat_n = size(data, 2);
if feat_n < window_size
    data_ = function_(data);
    return;
end

% calculate the start/end point for each window
st = 1:hop_size:feat_n;
ed = st + window_size - 1;
while ed(end) > feat_n
    st = st(1:end - 1);
    ed = ed(1:end - 1);
end
feat_n = length(st);

% apply fun to each window
data_ = cell(1, feat_n);
for i = 1:feat_n
    data_{i} = function_(data(:, st(i):ed(i)));
end
data_ = cell2mat(data_);