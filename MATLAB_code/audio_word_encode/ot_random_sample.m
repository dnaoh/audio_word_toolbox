function data = ot_random_sample(data, process_option)
% parse option
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Sampling Persentage')
        persentage = str2double(process_option{i + 1});
    end
end

% process the input data
data_length = size(data, 2);
random_vector = ceil(rand(data_length, 1) * data_length);
random_vector = sort(random_vector(1:max(round(persentage * data_length), 1)));
data = data(:, random_vector);