function data_ = en_vector_quantization(data, dictionary, process_option)
[~] = process_option; % process_option is not used in this function
data_ = sparse(size(dictionary, 2), size(data, 2));
for i = 1:size(data, 2)
    % distance computation
    distance = zeros(1, size(dictionary, 2));
    for j = 1:size(dictionary, 2)
        difference = data(:, i) - dictionary(:, j);
        distance(j) = sqrt(difference' * difference);
    end
    
    % select the nearest element in dictionary
    [~, distance_order] = sort(distance, 2, 'ascend');
    data_(distance_order(1), i) = distance(distance_order(1));
    data_(data_(:, i) > 0, i) = 1;
end