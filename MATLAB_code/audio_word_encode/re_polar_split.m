function data = re_polar_split(data, process_option)
[~] = process_option; % process_option is not used in this function
data_pos = data;
data_pos(data_pos<0) = 0;
data_neg = -data;
data_neg(data_neg<0) = 0;
data = [
    data_pos;
    data_neg;
    ];