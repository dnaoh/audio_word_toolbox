function data = ot_other_layer(data, process_option)
% parse process type
for i = 1:length(process_option)
    if strcmpi(process_option{i}, 'Process')
        process = process_option{i + 1};
    end
end

% process the input data accordingly
if strcmpi(process, 'Normalization')
    data = ot_normalization(data, process_option);
elseif strcmpi(process, 'Random Sampling')
    data = ot_random_sample(data, process_option);
elseif strcmpi(process, 'Consecutive Frame')
    data = ot_conscutive_frame(data, process_option);
end